<?php
require_once("includes/setup.php");
require_once("includes/sidebar.php");
require_once("includes/classes/Meet.php");
require_once("includes/classes/Member.php");
require_once("includes/classes/MeetSelector.php");
require_once("includes/classes/MeetEvent.php");
checkLogin();

addlog("Access", "Accessed relaylist.php");

if (isset($_POST['meetId'])) {

    $meetId = intval($_POST['meetId']);

}

if (isset($_POST['eventId'])) {

    $eventId = intval($_POST['eventId']);

}

htmlHeaders("Relay Entry List");

sidebarMenu();

echo "<div id=\"main\">\n";

echo "<h1>Relay Entries</h1>\n";

$event = new MeetEvent();
$event->load($eventId);
$eventTitle = $event->getShortDetails();

echo "<h2>$eventTitle</h2>\n";

echo "<script type=\"text/javascript\" src=\"relayentry.js\"></script>\n";

echo "<table class=\"list\" id=\"data\" width=\"100%\">\n";
echo "<thead class=\"list\">\n";
echo "<tr>\n";
echo "<th>\n";
echo "</th>\n";
echo "<th>\n";
echo "Club\n";
echo "</th>\n";
echo "<th>\n";
echo "Age\n";
echo "</th>\n";
echo "<th>\n";
echo "Team\n";
echo "</th>\n";
echo "<th>\n";
echo "1st Swimmer\n";
echo "</th>\n";
echo "<th>\n";
echo "2nd Swimmer\n";
echo "</th>\n";
echo "<th>\n";
echo "3rd Swimmer\n";
echo "</th>\n";
echo "<th>\n";
echo "4th Swimmer\n";
echo "</th>\n";
echo "</tr>\n";
echo "</thead>\n";
echo "<tbody class=\"list\" id=\"relayTeams\">\n";

echo "</tbody>\n";
echo "</table>\n";

?>

<script>

    $( document ).ready(function() {
        var url = new URL(url_string);
        var eventId = url.searchParams.get("eventId");

        getTeams(meetId, eventId);

    });

</script>

<?php

echo "</body>\n";
echo "</html>\n";
