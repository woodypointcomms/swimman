/**
 * Created by david on 2/11/16.
 */

var selectedMeetId;
var selectedEventId;
var selectedClubId;
var member_id;
var authorised;

var clubList = [];
var relayEvents = [];
var clubEntrants = [];

var meetsList = [];
var teamList = [];

var teamTable;

var editmode = false;

var RelayEvent = {
    id: 0,
    legs: 4,
    gender: '',
    distance: 0,
    stroke: ''
};

var RelayTeam = {
    id: 0,
    event: {},
    swimmers: [],
    age: 0
};

Date.createFromMysql = function (mysql_string) {
    var t, result = null;

    if (typeof mysql_string === 'string') {
        t = mysql_string.split(/[- :]/);

        //when t[3], t[4] and t[5] are missing they defaults to zero
        result = new Date(t[0], t[1] - 1, t[2], t[3] || 0, t[4] || 0, t[5] || 0);
    }

    return result;
}


var rowSelected = false;

// Only display loading on wrapper 2 in joomla
$body = $("wrapper2");
// $(document).on({
//     ajaxStart: function() { $body.addClass("loading");    },
//     ajaxStop: function() { $body.removeClass("loading"); }
// });

$('#data').DataTable({
    paging: false,
    searching: false
});

$("tbody.list").on("click", "tr", function () {

    var highlighted = $(this).hasClass("highlight");
    rowSelected = !rowSelected;

    if (!highlighted) {
        $(".list tr").removeClass("highlight");
    }

});

$('tbody.list').on("mouseenter", "tr", function () {

    if (rowSelected == false) {
        $(this).addClass('highlight');
    }

});

$('tbody.list').on("mouseleave", "tr", function () {

    if (rowSelected == false) {
        $(this).removeClass('highlight');
    }
});

$(document).ready(function () {

    member_id = $('#memberId').val();

    console.log("Got member id: " + member_id);

    $('#meetId').change(function () {
        selectedMeetId = $(this).val();
        console.log("Changed meetId to: " + selectedMeetId);
        getRelayEvents();
    });

    $('#eventId').change(function () {
        selectedEventId = $(this).val();
        console.log("Changed eventId to: " + selectedEventId);
        updateRelayForm();

        if (isEventMedley(selectedEventId)) {
            displayMedleyLabels();
        } else {
            displayOrdinalLabels();
        }

        console.log("eventChange: check if meet is open");
        if (meetIsOpen()) {

            // If meet is open, form is open
            $('#card-event-closed').hide();
            $('#card-no-relays-open').hide();
            $('#createTeamForm').show("slow");

        } else {

            console.log("eventChange: check if event is open");
            if (eventIsOpen(selectedEventId)) {

                // If meet is closed but event is open, form is open
                $('#card-event-closed').hide();
                $('#card-no-relays-open').hide();
                $('#createTeamForm').show("slow");

            } else {

                console.log("eventChange: check are events is open");
                if (eventsAreOpen()) {

                    // If meet is closed, this event is closed, but other events are open
                    $('#card-event-closed').show("slow");
                    $('#card-no-relays-open').hide();
                    $('#createTeamForm').hide();
                } else {

                    // Meet is closed an no events are open
                    $('#card-no-relays-open').show("slow");
                    $('#card-event-closed').hide();
                    $('#createTeamForm').hide();

                }
            }
        }

    });

    $('#clubId').change(function () {
        selectedClubId = $(this).val();
        $('#newTeamClub').val(selectedClubId);
        console.log("Changed clubId to: " + selectedClubId);
        updateRelayForm();

    });

    // Setup team member selection so you can't select the same person in more than one field
    $('#newTeamSwimmer1').change(function () {

        if ($('#newTeamSwimmer1').val() === $('#newTeamSwimmer2').val()) {
            $('#newTeamSwimmer2').val('');
        }

        if ($('#newTeamSwimmer1').val() === $('#newTeamSwimmer3').val()) {
            $('#newTeamSwimmer3').val('');
        }

        if ($('#newTeamSwimmer1').val() === $('#newTeamSwimmer4').val()) {
            $('#newTeamSwimmer4').val('');
        }

        updateRelayTeam();
    });

    $('#newTeamSwimmer2').change(function () {

        if ($('#newTeamSwimmer2').val() === $('#newTeamSwimmer1').val()) {
            $('#newTeamSwimmer1').val('');
        }

        if ($('#newTeamSwimmer2').val() === $('#newTeamSwimmer3').val()) {
            $('#newTeamSwimmer3').val('');
        }

        if ($('#newTeamSwimmer2').val() === $('#newTeamSwimmer4').val()) {
            $('#newTeamSwimmer4').val('');
        }

        updateRelayTeam();
    });

    $('#newTeamSwimmer3').change(function () {

        if ($('#newTeamSwimmer3').val() === $('#newTeamSwimmer1').val()) {
            $('#newTeamSwimmer1').val('');
        }

        if ($('#newTeamSwimmer3').val() === $('#newTeamSwimmer2').val()) {
            $('#newTeamSwimmer2').val('');
        }

        if ($('#newTeamSwimmer3').val() === $('#newTeamSwimmer4').val()) {
            $('#newTeamSwimmer4').val('');
        }

        updateRelayTeam();
    });

    $('#newTeamSwimmer4').change(function () {

        if ($('#newTeamSwimmer4').val() === $('#newTeamSwimmer2').val()) {
            $('#newTeamSwimmer2').val('');
        }

        if ($('#newTeamSwimmer4').val() === $('#newTeamSwimmer3').val()) {
            $('#newTeamSwimmer3').val('');
        }

        if ($('#newTeamSwimmer4').val() === $('#newTeamSwimmer1').val()) {
            $('#newTeamSwimmer1').val('');
        }

        updateRelayTeam();
    });

    $('#newAgeGroup').change(function () {
        updateRelayTeam();
    });

    $('#newTeamLetter').change(function () {
        updateRelayTeam();
    });

    $('#newTeamSeedTime').blur(function () {
        var entered = $('#newTeamSeedTime').val();
        $('#newTeamSeedTime').val(rewriteTime(entered));
    });

    $('#newTeamCreate').click(function () {
        createTeam();
    });

    $('#resetTeamCreate').click(function () {
        resetTeam();
    });

    $('#editTeamSubmit').click(function() {
        editTeamSubmit();
    });

    // Check if this user has access
    hasAccess();

});


$.ajaxSetup({
    beforeSend: function () {
        console.log("Start loading...");
        $('#main').addClass('loading');
    },
    complete: function () {
        console.log("Stop loading...");
        $('#main').removeClass('loading');
    }
});

function isEventMedley(eventId) {

    // Check if any events are still open
    var eventDetails = relayEvents.find(x => x.id === eventId);

    if (eventDetails.discipline == "Medley") {
        // console.log("Event " + eventId + " is a medley event!");
        return true;
    } else {
        // console.log("Event " + eventId + " is not a medley event!");
        return false;
    }

}

function displayMedleyLabels() {

    $('#lblSwimmer1').text("Backstroke:");
    $('#lblSwimmer2').text("Breaststroke:");
    $('#lblSwimmer3').text("Butterfly:");
    $('#lblSwimmer4').text("Freestyle:");

    $('#tblHeadSwimmer1').text("Backstroke");
    $('#tblHeadSwimmer2').text("Breaststroke");
    $('#tblHeadSwimmer3').text("Butterfly");
    $('#tblHeadSwimmer4').text("Freestyle");

    var header1 = teamTable.columns(3).header();
    var header2 = teamTable.columns(4).header();
    var header3 = teamTable.columns(5).header();
    var header4 = teamTable.columns(6).header();

    $(header1).html('Backstroke');
    $(header2).html('Breaststroke');
    $(header3).html('Butterfly');
    $(header4).html('Freestyle');

}

function displayOrdinalLabels() {

    $('#lblSwimmer1').text("Swimmer 1:");
    $('#lblSwimmer2').text("Swimmer 2:");
    $('#lblSwimmer3').text("Swimmer 3:");
    $('#lblSwimmer4').text("Swimmer 4:");

    $('#tblHeadSwimmer1').text("Swimmer 1");
    $('#tblHeadSwimmer2').text("Swimmer 2");
    $('#tblHeadSwimmer3').text("Swimmer 3");
    $('#tblHeadSwimmer4').text("Swimmer 4");

    var header1 = teamTable.columns(3).header();
    var header2 = teamTable.columns(4).header();
    var header3 = teamTable.columns(5).header();
    var header4 = teamTable.columns(6).header();
    $(header1).html('Swimmer 1');
    $(header2).html('Swimmer 2');
    $(header3).html('Swimmer 3');
    $(header4).html('Swimmer 4');

}

function resetTeam() {

    // reset team
    $('#newTeamCreate').prop('disabled', true);

    RelayTeam = {
        id: 0,
        event: {},
        swimmers: [],
        age: 0
    };

    $("#newAgeGroup").val("");
    $("#newTeamLetter").val("");
    $("#newTeamName").val("");
    $("#newTeamSeedTime").val("");
    $("#newTeamSwimmer1").val("");
    $("#newTeamSwimmer2").val("");
    $("#newTeamSwimmer3").val("");
    $("#newTeamSwimmer4").val("");

    $('#newTeamLetter')
        .empty()
        .append('<option value="">Automatically select</option>')
        .append('<option value="A">A</option>')
        .append('<option value="B">B</option>')
        .append('<option value="C">C</option>')
        .append('<option value="D">D</option>')
        .append('<option value="E">E</option>');

    updateAgeOfTeam();
    checkGenders();

    // Change header
    $('#createTeamHeader').text("Create a Team");
    $('#newTeamCreate').show();
    $('#editTeamSubmit').hide();

    $("html, body").animate({scrollTop: $('#createTeamForm').offset().top}, 1000);

}

function updateRelayTeam() {

    var swimmerIds = [];
    swimmerIds[0] = $('#newTeamSwimmer1').val();
    swimmerIds[1] = $('#newTeamSwimmer2').val();
    swimmerIds[2] = $('#newTeamSwimmer3').val();
    swimmerIds[3] = $('#newTeamSwimmer4').val();

    // Empty relay team
    RelayTeam.swimmers = [];

    swimmerIds.forEach(function (swimmerId, idx) {

        // console.log("updateRelayTeam swimmerId = " + swimmerId);

        if (swimmerId !== '') {

            var selectedSwimmer = clubEntrants.find(x => x.id === swimmerId);

            // console.log('selectedSwimmer: ' + selectedSwimmer);

            if (selectedSwimmer != null) {

                var teamMember = {
                    id: selectedSwimmer.id,
                    leg: idx + 1,
                    firstname: selectedSwimmer.firstname,
                    surname: selectedSwimmer.surname,
                    gender: selectedSwimmer.gender,
                    age: selectedSwimmer.age
                };

                RelayTeam.swimmers.push(teamMember);

            }

        }

    });

    updateAgeOfTeam();
    checkGenders();

    if (getNumberOfSwimmers() == 4) {
        checkLetter();
    } else {

        if (($('#newTeamLetter').val()) !== '' && ($('#newAgeGroup').val() !== '')) {
            checkLetter();
        } else {
            $('#card-letter-used').hide();
        }
    }
}

function getNumberOfSwimmers() {

    var numSwimmers = 0;

    RelayTeam.swimmers.forEach(function (swimmer) {
        numSwimmers += 1;
    });

    return numSwimmers;

}


function updateAgeOfTeam() {

    var intTotalAge = 0;
    var numSwimmers = 0;

    RelayTeam.swimmers.forEach(function (swimmer) {
        intTotalAge += parseInt(swimmer.age);
        numSwimmers += 1;
    });

    RelayTeam.age = intTotalAge;
    $('#totalAge').val(intTotalAge);

    if (numSwimmers === 4) {
        checkAgeGroup();
    }

}

function checkAgeGroup() {

    var selectedAgeGroupBase = $('#newAgeGroup').val();

    if (selectedAgeGroupBase !== '') {

        var minAge = 0;
        var maxAge = 0;

        switch (parseInt(selectedAgeGroupBase)) {
            case 72:
                minAge = 72;
                maxAge = 119;
                break;
            case 120:
                minAge = 120;
                maxAge = 159;
                break;
            case 160:
                minAge = 160;
                maxAge = 199;
                break;
            case 200:
                minAge = 200;
                maxAge = 239;
                break;
            case 240:
                minAge = 240;
                maxAge = 279;
                break;
            case 280:
                minAge = 280;
                maxAge = 319;
                break;
            case 320:
                minAge = 320;
                maxAge = 359;
                break;
            case 360:
                minAge = 360;
                maxAge = 399;
                break;
        }

        // console.log("Age group selected: min=" + minAge + " max=" + maxAge + " age=" + RelayTeam.age);

        if ((minAge <= RelayTeam.age) && (RelayTeam.age <= maxAge)) {
            // console.log("Relay team age group correct");
            $('#card-age-incorrect').hide();
            $('#newTeamCreate').prop('disabled', false);
        } else {
            $('#newTeamCreate').prop('disabled', true);
            $('#card-age-incorrect').show("slow");
            // console.log("Relay team age incorrect");
        }

    } else {
        $('#card-age-incorrect').hide();
        $('#newTeamCreate').prop('disabled', false);
        // console.log('Automatically select age group');
    }

}

function getAgeGroup() {

    var ageGroup = 0;

    if ((RelayTeam.age >= 72) && (RelayTeam.age <= 119)) {
        ageGroup = "72-199";
    }

    if ((RelayTeam.age >= 120) && (RelayTeam.age <= 159)) {
        ageGroup = "120-159";
    }

    if ((RelayTeam.age >= 160) && (RelayTeam.age <= 199)) {
        ageGroup = "160-199";
    }

    if ((RelayTeam.age >= 200) && (RelayTeam.age <= 239)) {
        ageGroup = "200-239";
    }

    if ((RelayTeam.age >= 240) && (RelayTeam.age <= 279)) {
        ageGroup = "240-279";
    }

    if ((RelayTeam.age >= 280) && (RelayTeam.age <= 319)) {
        ageGroup = "280-319";
    }

    if ((RelayTeam.age >= 320) && (RelayTeam.age <= 359)) {
        ageGroup = "320-359";
    }

    if ((RelayTeam.age >= 360) && (RelayTeam.age <= 399)) {
        ageGroup = "360-399";
    }

    return ageGroup;

}

function checkLetter() {

    // console.log("checkLetter");

    if (editmode) {
        $('#card-letter-used').hide();
        $('#newTeamCreate').prop('disabled', false);
        return;
    }

    var letter = $('#newTeamLetter').val();
    var selectedAge = $('#newAgeGroup').val();

    var ageGroup = "";

    if (selectedAge !== '') {
        switch (parseInt(selectedAge)) {
            case 72:
                ageGroup = "72-119";
                break;
            case 120:
                ageGroup = "120-159";
                break;
            case 160:
                ageGroup = "160-199";
                break;
            case 200:
                ageGroup = "200-239";
                break;
            case 240:
                ageGroup = "240-279";
                break;
            case 280:
                ageGroup = "280-319";
                break;
            case 320:
                ageGroup = "320-359";
                break;
            case 360:
                ageGroup = "360-399";
                break;
        }
    } else {
        ageGroup = getAgeGroup();
    }

    if (letter !== '') {

        var url = "/swimman/json/relayteams.php?meetId=" + selectedMeetId + "&eventId=" + selectedEventId + "&clubId=" + selectedClubId;
        $.getJSON(url, {
            format: "json"
        })
            .done(function (data) {

                var letterUsed = false;

                data.forEach(function (team) {
                    if (team.groupname.split(' ')[1] === ageGroup) {

                        console.log(team.letter + " " + letter);
                        if (team.letter === letter) {
                            letterUsed = true;
                        }

                    }
                });

                // console.log("letterused = " + letterUsed);

                if (letterUsed) {
                    $('#card-letter-used').show();
                    $('#newTeamCreate').prop('disabled', true);
                } else {
                    $('#card-letter-used').hide();
                    $('#newTeamCreate').prop('disabled', false);
                }

            });

    } else {
        $('#card-letter-used').hide();
        $('#newTeamCreate').prop('disabled', false);
    }

}

function checkGenders() {

    // Is this a mixed event?
    var eventDetails = relayEvents.find(x => x.id === selectedEventId);

    if (parseInt(eventDetails.gender) === 3) {

        var numMale = 0;
        var numFemale = 0;
        var numSwimmers = 0;

        RelayTeam.swimmers.forEach(function (swimmer) {
            if (swimmer.gender === 'M') numMale += 1;
            if (swimmer.gender === 'F') numFemale += 1;
            numSwimmers += 1;
        });

        if (numSwimmers === 4) {
            if (numMale !== 2 || numFemale !== 2) {
                $('#card-gender-incorrect').show();
            } else {
                $('#card-gender-incorrect').hide();
            }
        } else {
            $('#card-gender-incorrect').hide();
        }
    } else {
        $('#card-gender-incorrect').hide();
    }

}

function hasAccess() {

    var url = "/swimman/json/clubaccess.php?member_id=" + member_id;
    $.getJSON(url, {
        format: "json"
    })
        .done(function (data) {

            if (data.length === 0) {
                authorised = false;

                $('#card-no-access').show();

            } else {

                authorised = true;
                clubList = data;

                console.log(clubList);

                $.each(clubList, function (idx, clubDetails) {
                    $('#clubId').append('<option value=\"' + clubDetails.club_id + '\">' + clubDetails.clubname + '</option>');
                });

                $('#clubId').val($('#clubId option:first').val());
                selectedClubId = $('#clubId option:selected').val();
                $('#clubId').trigger('change');

                if (clubList.length > 1) {
                    $('#card-select-club').show();
                    $('#clubIdField').css('display', 'flex');
                }

                getCurrentMeets();

            }

        });

}

function getCurrentMeets() {

    var url = "/swimman/json/currentmeets.php";
    $.getJSON(url, {
        format: "json"
    })
        .done(function (data) {

            // Empty the meet list
            meetsList = [];

            // Does this event have relays?
            $.each(data, function (key, value) {

                var relayEvents = new Array();

                for (var i = 0; i < value.events.length; i++) {

                    if (parseInt(value.events[i].legs) === 4) {
                        relayEvents.push(value.events[i]);
                    }

                }

                // Add to drop down
                if (relayEvents.length > 0) {
                    meetsList.push(value);
                    $('#meetId').append('<option value=\"' + value.id + '\">' + value.meetname + '</option>');
                }

            });

            // Decide if we show stuff
            if (authorised) {

                if (meetsList.length === 0) {
                    $('#card-no-meets').show("slow");
                } else {
                    $('#card-no-meets').hide();
                    $('#card-select-meet-event').show("slow");
                    $('#newRelayForm').show("slow");
                }

                // Select the first meet
                $('#meetId').val($('#meetId option:first').val());
                selectedMeetId = $('#meetId option:selected').val();
                console.log("getCurrentMeets: selectedMeetId: " + selectedMeetId);
                $('#meetId').trigger('change');

            }

        });

}

function getRelayEvents() {

    // console.log("getRelayEvents");

    if (selectedMeetId !== undefined) {

        $.each(meetsList, function (idx, meetDetails) {

            if (meetDetails.id === selectedMeetId) {

                $('#eventId').empty();
                relayEvents = [];

                $.each(meetDetails.events, function (idx, eventDetails) {

                    if (eventDetails.legs > 1) {

                        if (parseInt(eventDetails.gender) === 1) {
                            eventGender = "Men's";
                        } else if (parseInt(eventDetails.gender) === 2) {
                            eventGender = "Women's";
                        } else {
                            eventGender = "Mixed";
                        }

                        // Populate the events drop down
                        var eventId = eventDetails.id;
                        var eventDesc = "#" + eventDetails.prognumber + eventDetails.progsuffix;
                        eventDesc += " " + eventDetails.legs + "x" + eventDetails.distance;
                        eventDesc += " " + eventGender + " " + eventDetails.discipline;

                        // Populate global model of this meet's events
                        relayEvents.push(eventDetails);

                        $('#eventId')
                            .append('<option value="' + eventId + '">' + eventDesc + '</option>');

                    }
                });

            }

        });

        // Make default first event selection
        $('#eventId').val($('#eventId option:first').val()).trigger("change");
        selectedEventId = $('#eventId option:selected').val();
        // console.log("getRelayEvents: selectedEventId: " + selectedEventId);

    }

}

function eventsAreOpen() {

    // console.log("eventsAreOpen()");

    // TODO: handle TZ
    if (!meetIsOpen()) {

        // Check if any events are still open
        var eventsOpen = false;
        relayEvents.forEach(function (eventDetails) {
            if (eventIsOpen(eventDetails.id)) {
                eventsOpen = true;
            }
        });

        if (eventsOpen) {
            // console.log("eventsAreOpen(): returns true")
            return true;
        }

        // console.log("eventsAreOpen(): no events are open");

        return false

    } else {
        // console.log("eventsAreOpen(): events are open");

        return true;
    }

}

function eventIsOpen(eventId) {

    // console.log("eventIsOpen: " + eventId);

    // Check if any events are still open
    var eventDetails = relayEvents.find(x => x.id === eventId);

    if (eventDetails.deadline === null) {

        // console.log("eventIsOpen: no event deadline defined.");
        return false;
    }

    var deadline = Date.createFromMysql(eventDetails.deadline + " 23:59:59");

    return deadline > Date.now();

}

function meetIsOpen() {
    var meetDetails = meetsList.find(x => x.id === selectedMeetId);
    var deadline = Date.createFromMysql(meetDetails.deadline + " 23:59:59");

    // TODO: handle TZ
    if (Date.now() > deadline) {

        // console.log("meetIsOpen(): meet is closed");

        return false
    } else {
        // console.log("meetIsOpen(): meet is open");

        return true;
    }

}

function getAvailableSwimmers() {

    // console.log('getAvailableSwimmers');

    if (selectedMeetId !== undefined && selectedEventId !== undefined && selectedClubId !== undefined) {

        var eventDetails = relayEvents.find(x => x.id === selectedEventId);
        var eventGender;

        if (parseInt(eventDetails.gender) === 1) {
            eventGender = "M";
        } else if (parseInt(eventDetails.gender) === 2) {
            eventGender = "F";
        } else {
            eventGender = "X";
        }

        // console.log('Requesting available ' + eventGender + ' swimmers for meetId=' + selectedMeetId + ' eventId=' + selectedEventId +
        //     ' clubId=' + clubId);

        var url = "/swimman/json/entrants.php?meetId=" + selectedMeetId + "&eventId=" + selectedEventId + '&clubId=' + selectedClubId;
        $.getJSON(url, {
            format: "json"
        })
            .done(function (data) {

                clubEntrants = data;

                $('.newTeamSwimmer')
                    .find('optgroup')
                    .remove()
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value=""></option>');

                var nominatedEntrants = data.filter(entrant => parseInt(entrant.nominated) === 1);
                var availableEntrants = data.filter(entrant => parseInt(entrant.nominated) === 0);

                console.log("nominatedEntrants");
                console.log(nominatedEntrants);

                console.log("availableEntrants");
                console.log(availableEntrants);

                if (nominatedEntrants.length > 0) {
                    $('.newTeamSwimmer').append('<optgroup class="nominated-swimmer" label=\'Nominated for this relay event\'></optgroup>');

                    $.each(nominatedEntrants, function (key, value) {

                        var swimmerId = value.id;
                        var swimmerAge = value.gender + value.age;

                        var swimmerName = " " + value.surname + ", " + value.firstname + " (" + swimmerAge + ")";

                        if ((eventGender === "X") || (eventGender === value.gender)) {

                            $('.newTeamSwimmer optgroup.nominated-swimmer')
                                .append('<option value="' + swimmerId + '">' + swimmerName + '</option>');

                        }

                    });

                }

                if (availableEntrants.length > 0) {
                    $('.newTeamSwimmer').append('<optgroup class="available-swimmer" label=\'Available for this relay event\'></optgroup>');

                    $.each(availableEntrants, function (key, value) {

                        var swimmerId = value.id;
                        var swimmerAge = value.gender + value.age;

                        var swimmerName = " " + value.surname + ", " + value.firstname + " (" + swimmerAge + ")";

                        if ((eventGender === "X") || (eventGender === value.gender)) {

                            $('.newTeamSwimmer optgroup.available-swimmer')
                                .append('<option value="' + swimmerId + '">' + swimmerName + '</option>');

                        }

                    });

                }

            });

    }

}

function getTeams() {

    // console.log('getTeams for meetId=' + selectedMeetId + ' eventId=' + selectedEventId + ' clubId=' + selectedClubId);

    if (selectedMeetId != undefined && selectedEventId != undefined && selectedClubId != undefined) {

        console.log('Requesting teams for meetId=' + selectedMeetId + ' eventId=' + selectedEventId + ' clubId=' + selectedClubId)

        var url = "/swimman/json/relayteams.php?meetId=" + selectedMeetId + "&eventId=" + selectedEventId + "&clubId=" + selectedClubId;

        teamTable = $('#data').DataTable({
            "paging": false,
            "searching": false,
            "ajax": url,
            "sAjaxDataProp": "",
            "destroy": "true",
            "columns": [
                {
                    "data": function (json) {
                        var output = "<a href='#' onclick='editTeam(" + json.id + ")'>" +
                            "<img src=\"/swimman/images/edit.png\" alt='Edit Team' /></a>" +
                            "<a href='#' onclick='deleteTeam(" + json.id + ")'>" +
                            "<img src=\"/swimman/images/delete.png\" alt='Delete Team' /></a>";
                        // var output = "<a href='#' onclick='deleteTeam(" + json.id + ")'>" +
                        //     "<img src=\"/swimman/images/delete.png\" alt='Delete Team' /></a>";
                        return output;
                    }
                },
                // { "data": function (json) {
                //     var clubInfo;
                //     clubInfo = "<abbr title=\"" + json.clubname + "\">" + json.code + "</abbr>";
                //     return clubInfo;
                // }, className : "dt-center" },
                {
                    "data": function (json) {
                        var groupName = json.groupname.split(' ');
                        return "<abbr title=\"" + json.groupname + "\">" + groupName[1] + "</abbr>";
                    }, className: "dt-center"
                },
                {
                    "data": function (json) {
                        var teamNameOut = json.letter;
                        if (json.teamname != null && json.teamname != '') {
                            teamNameOut += ' - ' + json.teamname;
                        }
                        return teamNameOut;
                    }, className: "dt-center"
                },
                {
                    "data": 'swimmer1name',
                    "defaultContent": 'n/a'
                },
                {
                    "data": 'swimmer2name',
                    "defaultContent": 'n/a'
                },
                {
                    "data": 'swimmer3name',
                    "defaultContent": 'n/a'
                },
                {
                    "data": 'swimmer4name',
                    "defaultContent": 'n/a'
                }
            ]
        });

    }

}

function updateRelayForm() {

    RelayTeam.swimmers = [];
    RelayTeam.id = 0;
    RelayTeam.event = 0;

    getTeams();
    getAvailableSwimmers();

}

function createTeam() {

    // console.log("Create team for meetId=" + selectedMeetId + " eventId=" + selectedEventId + " clubId=" + selectedClubId);

    console.log($("#newRelayForm").serialize());

    $.post("/swimman/json/createrelay.php", $("#newRelayForm").serialize())
        .done(function (data) {

            console.log(data);

            resetTeam();
            updateRelayForm();

            $("html, body").animate({scrollTop: $('#headingRelayTeams').offset().top}, 1000);
        });

}

function setEventGender() {

    var eventName = $('#eventId option:selected').text();

    // console.log("eventId changed to " + eventId + " " + eventName);

    if (eventName.includes("Women")) {
        eventGender = "F";
        // console.log("Womens event selected");
    } else if (eventName.includes("Men")) {
        eventGender = "M";
        // console.log("Mens event selected");
    } else {
        eventGender = "X";
        // console.log("Mixed event selected");
    }

}

function deleteTeam(teamId) {

    var retVal = confirm("Are you sure you want to delete this relay team?");

    if (retVal == true) {

        // console.log("Delete requested for teamId=" + teamId);

        $.post("/swimman/json/deleterelay.php", {teamId: teamId})
            .done(function (data) {
                getTeams(meetId, eventId);
                getAvailableSwimmers(meetId, eventId, eventGender, clubId);
            });
    }

}

/**
 * Loads a team for editing
 *
 * @param teamId to edit
 */
function editTeam(teamId) {

    // Change header
    $('#createTeamHeader').text("Edit a Team");
    $('#newTeamCreate').hide();
    $('#editTeamSubmit').show();

    editmode = true;

    // Set the existing team id
    $('#relayId').val(teamId);

    // console.log("editTeam= " + teamId);
    // console.log("relayId set to " + $('#relayId').val());

    // Get team details
    var url = "/swimman/json/relayteams.php?meetId=" + selectedMeetId + "&eventId=" + selectedEventId + "&clubId=" + selectedClubId;

    $.get(url).done(function (data) {

        console.log(data);

        data.forEach(function (teamRow, idx) {

            if (parseInt(teamRow.id) === teamId) {

                $('#newTeamClub').val(teamRow.clubid);
                $('#newTeamClub').trigger('change');
                $('#newTeamName').val(teamRow.teamname);
                $('#newTeamName').trigger('change');

                var groupId = teamRow.groupname.split(' ')[1].split('-')[0];

                console.log(groupId);

                $('#newAgeGroup').val(groupId);
                $('#newAgeGroup').trigger('change');

                $('#newTeamLetter').val(teamRow.letter);
                $('#newTeamLetter').trigger('change');

                $('.newTeamSwimmer').append('<optgroup class="existing-swimmer" label=\'Currently in this relay team\'></optgroup>');

                var swimmer1age = teamRow.swimmer1gender + teamRow.swimmer1age;
                var swimmer1name = " " + teamRow.swimmer1name + " (" + swimmer1age + ")";
                var swimmer2age = teamRow.swimmer2gender + teamRow.swimmer2age;
                var swimmer2name = " " + teamRow.swimmer2name + " (" + swimmer2age + ")";
                var swimmer3age = teamRow.swimmer3gender + teamRow.swimmer3age;
                var swimmer3name = " " + teamRow.swimmer3name + " (" + swimmer3age + ")";
                var swimmer4age = teamRow.swimmer4gender + teamRow.swimmer4age;
                var swimmer4name = " " + teamRow.swimmer4name + " (" + swimmer4age + ")";

                console.log(swimmer1name);

                $('.newTeamSwimmer optgroup.existing-swimmer')
                    .append('<option value="' + teamRow.swimmer1id + '">' + swimmer1name +
                        '</option>');
                $('.newTeamSwimmer optgroup.existing-swimmer')
                    .append('<option value="' + teamRow.swimmer2id + '">' + swimmer2name +
                        '</option>');
                $('.newTeamSwimmer optgroup.existing-swimmer')
                    .append('<option value="' + teamRow.swimmer3id + '">' + swimmer3name +
                        '</option>');
                $('.newTeamSwimmer optgroup.existing-swimmer')
                    .append('<option value="' + teamRow.swimmer4id + '">' + swimmer4name +
                        '</option>');

                var existingMember1 = {
                    id: teamRow.swimmer1id,
                    firstname: teamRow.swimmer1firstname,
                    surname: teamRow.swimmer1surname,
                    gender: teamRow.swimmer1gender,
                    dob: '',
                    age: teamRow.swimmer1age,
                    clubId: selectedClubId,
                    code: '',
                    clubname: ''
                };

                var existingMember2 = {
                    id: teamRow.swimmer2id,
                    firstname: teamRow.swimmer2firstname,
                    surname: teamRow.swimmer2surname,
                    gender: teamRow.swimmer2gender,
                    dob: '',
                    age: teamRow.swimmer2age,
                    clubId: selectedClubId,
                    code: '',
                    clubname: ''
                };

                var existingMember3 = {
                    id: teamRow.swimmer3id,
                    firstname: teamRow.swimmer3firstname,
                    surname: teamRow.swimmer3surname,
                    gender: teamRow.swimmer3gender,
                    dob: '',
                    age: teamRow.swimmer3age,
                    clubId: selectedClubId,
                    code: '',
                    clubname: ''
                };

                var existingMember4 = {
                    id: teamRow.swimmer4id,
                    firstname: teamRow.swimmer4firstname,
                    surname: teamRow.swimmer4surname,
                    gender: teamRow.swimmer4gender,
                    dob: '',
                    age: teamRow.swimmer4age,
                    clubId: selectedClubId,
                    code: '',
                    clubname: ''
                };

                clubEntrants.push(existingMember1);
                clubEntrants.push(existingMember2);
                clubEntrants.push(existingMember3);
                clubEntrants.push(existingMember4);

                $('#newTeamSwimmer1').val(teamRow.swimmer1id);
                $('#newTeamSwimmer1').trigger('change');

                $('#newTeamSwimmer2').val(teamRow.swimmer2id);
                $('#newTeamSwimmer2').trigger('change');

                $('#newTeamSwimmer3').val(teamRow.swimmer3id);
                $('#newTeamSwimmer3').trigger('change');

                $('#newTeamSwimmer4').val(teamRow.swimmer4id);
                $('#newTeamSwimmer4').trigger('change');

                $('#newTeamSeedTime').val(teamRow.seedtime);

                console.log("team swimer 1: " + $('#newTeamSwimmer1').val());

                updateRelayTeam();

                console.log(RelayTeam);

            }

        });

    });

    $("html, body").animate({scrollTop: $('#createTeamForm').offset().top}, 1000);

}

function editTeamSubmit() {

    // console.log("Edit team for meetId=" + selectedMeetId + " eventId=" + selectedEventId + " clubId=" + selectedClubId);

    // console.log($("#newRelayForm").serialize());

    editmode = false;

    $.post("/swimman/json/editrelay.php", $("#newRelayForm").serialize())
        .done(function (data) {

            // console.log("editrelay team");
            //
            // console.log(data);

            resetTeam();
            updateRelayForm();

            $("html, body").animate({ scrollTop: $('#headingRelayTeams').offset().top }, 1000);

        });

}

function downloadEntries(meetId, eventId) {

    // console.log("Download request for meetid=" + meetId + " eventid=" + eventId);

    window.location.href = "gettmentries.php?meet=" + meetId + "&event=" + eventId + "&relaysonly=1";

}