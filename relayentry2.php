<?php
require_once("includes/setup.php");
require_once("includes/sidebar.php");
require_once("includes/classes/Meet.php");
require_once("includes/classes/Member.php");
require_once("includes/classes/MeetSelector.php");
require_once("includes/classes/MeetEvent.php");
checkLogin();

addlog("Access", "Accessed relayentry.php");

if (isset($_POST['meetId'])) {

    $meetId = intval($_POST['meetId']);

}

if (isset($_POST['eventId'])) {

    $eventId = intval($_POST['eventId']);

}

htmlHeaders("Relay Entries");



?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<!--<link rel="stylesheet" href="/swimman/style/jquery-ui.min.css"/>-->
<!--<link rel="stylesheet" href="/swimman/style/jquery-ui.structure.min.css"/>-->
<!--<link rel="stylesheet" href="/swimman/style/jquery-ui.theme.min.css"/>-->
<link rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css"/>

<!-- Latest compiled and minified CSS -->
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">-->


<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>-->

<script type="text/javascript" src="entrymanager.js"></script>
<script type="text/javascript" src="relayentry2.js"></script>

<style type="text/css">

    /*#clubIdField {*/
        /*display: none;*/
    /*}*/

    #card-select-club {
        display: none;
    }

    #card-select-meet-event {
        display: none;
    }

    #newRelayForm {
        display: none;
    }

    #card-no-meets {
        display: none;
    }

    #card-no-access {
        display: none;
    }

    #createTeamForm {
        display: none;
    }

    #card-gender-incorrect {
        display: none;
    }

    #card-age-incorrect {
        display: none;
    }

    #card-letter-used {
        display: none;
    }

    #editTeamSubmit {
        display: none;
    }

    a#card {
        color: #ffee58;
    }

    h2 {
        margin-top: 1em;
    }

    /* When the body has the loading class, we turn
the scrollbar off with overflow:hidden */
    div#main.loading {
        overflow: hidden;
    }

    /* Anytime the body has the loading class, our
	modal element will be visible */
    div#main.loading .loading-model {
        display: block;
    }

    .loading-model {
        display:    none;
        position:   fixed;
        z-index:    1000;
        top:        0;
        left:       0;
        height:     100%;
        width:      100%;
        background: rgba( 255, 255, 255, .8 )
        url('/swimman/images/ajaxload.gif')
        50% 50%
        no-repeat;
    }

</style>


<h1>Manage Relay Entries</h1>

<div class="card mb-3 bg-danger text-white" id="card-no-access">
    <div class="card-body">
        <h5 class="card-title">
            You do not have access to manage relay teams for any club!
        </h5>
        <p class="card-text">
            If you believe this is incorrect and you should have access to manage
            relay teams, please contact the Recording Subcommittee:
            <a href="mailto:recorder@mastersswimmingqld.org.au">recorder@mastersswimmingqld.org.au</a>.
        </p>
    </div>
</div>


<div class="card mb-3 bg-info text-white" id="card-select-meet-event">
    <div class="card-body">
        <p class="card-text">
            Select the Meet and Relay Event you'd like to view or add relay teams to.
        </p>
        <p class="card-text">
            <strong>Only meets that have relay events are shown!</strong>
        </p>
    </div>
</div>

<div class="card mb-3 bg-danger text-white" id="card-no-meets">
    <div class="card-body">
        <h5 class="card-title">No relay events are current available!</h5>
        <p class="card-text">
            <strong>Only meets that have relay events are shown!</strong>
        </p>
        <p class="card-text">
            Past meet relay teams are shown on the
            <a href="https://forum.mastersswimmingqld.org.au/entry-manager-new/club-entries">Club Entries</a> page.
            If you can't see a meet you expect to find here, check with
            the Meet Director to find out how to lodge relay entries for
            that meet.
        </p>
    </div>
</div>

<form method="post" id="newRelayForm">

	<?php
	echo "<input type='hidden' name='memberId' id='memberId' value='$memberId' />\n";
	?>

    <input type="hidden" name="newTeamClub" id="newTeamClub" value="" />

    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right">Meet: </label>
        <div class="col-sm-10">
            <select name="meetId" class="form-control" id="meetId">
            </select>
        </div>
    </div>
    <div class="form-group row">

        <label for="eventId" class="col-sm-2 col-form-label text-right">Event: </label>
        <div class="col-sm-10">
            <select name="eventId" class="form-control" id="eventId">
            </select>
        </div>

    </div>

    <div class="card mb-3 bg-info text-white" id="card-select-club">
        <div class="card-body">
            <p class="card-text">
                Select the Club you'd like to view or add relays for.
            </p>
            <p class="card-text">
                <strong>Only clubs that you have Club Captain or Club Recorder access to are shown!</strong>
            </p>
        </div>
    </div>

    <div class="form-group row" id="clubIdField">
        <label for="clubId" class="col-sm-2 col-form-label text-right">Club: </label>
        <div class="col-sm-10">
            <select name="clubId" class="form-control" id="clubId">
            </select>
        </div>
    </div>

    <h2 id="headingRelayTeams">Relay Teams</h2>

    <table class="list" id="data" width="100%">
        <thead class="list">
        <tr>
            <th>
            </th>
            <th>
                Age
            </th>
            <th>
                Team
            </th>
            <th id="tbnHeadSwimmer1">
                1st Swimmer
            </th>
            <th id="tbnHeadSwimmer2">
                2nd Swimmer
            </th>
            <th id="tbnHeadSwimmer3">
                3rd Swimmer
            </th>
            <th id="tbnHeadSwimmer4">
                4th Swimmer
            </th>
        </tr>
        </thead>
        <tbody class="list" id="relayTeams">

        </tbody>
    </table>

    <div class="card mb-3 bg-warning text-white" id="card-no-relays-open">
        <div class="card-body">
            <h5>No Relay Events open</h5>
            <p class="card-text">
                No relay events in this meet are currently accepting entries. Please check with
                the Meet Director if you want to lodge or amend a relay team.
            </p>
        </div>
    </div>

    <div class="card mb-3 bg-warning text-white" id="card-event-closed">
        <div class="card-body">
            <h5>This Relay Event is closed</h5>
            <p class="card-text">
                This relay event is now closed. Please check with
                the Meet Director if you want to lodge or amend a relay team.
            </p>
        </div>
    </div>

    <div id="createTeamForm">

        <h2 id='createTeamHeader'>Create a Team</h2>

        <div class="card mb-3 bg-info text-white">
            <div class="card-body">
                <p class="card-text">
                    To allow Entry Manager to automatically determine the correct
                    age group and/or relay letter for your team, use the <strong>Automatically select</strong>
                    options for Age Group and/or Team Letter.
                </p>
                <p>
                    If you select an Age Group for your team, you will not be able to lodge the team if
                    the relay team member's age's do not add up to the correct age range.
                </p>
                <p>
                    <strong>Note: </strong> The first relay team in <strong>each</strong> age group should have the Team
                    Letter <strong>A</strong>. If you select "Automatically select", Entry Manager will handle this for
                    you.
                </p>
            </div>
        </div>

        <div class="form-group row">
            <label for="newAgeGroup" class="col-sm-2 col-form-label text-right">Age Group: </label>
            <div class="col-sm-10">
                <select name="newAgeGroup" id="newAgeGroup" class="form-control">
                    <option value="">Automatically select</option>
                    <option value="72">72-119</option>
                    <option value="120">120-159</option>
                    <option value="160">160-199</option>
                    <option value="200">200-239</option>
                    <option value="240">240-279</option>
                    <option value="280">280-319</option>
                    <option value="320">320-359</option>
                    <option value="360">360-399</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="newTeamLetter" class="col-sm-2 col-form-label text-right">Team Letter: </label>
            <div class="col-sm-10">
                <select name="newTeamLetter" id="newTeamLetter" class="form-control">
                    <option value="">Automatically select</option>
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>
                    <option value="D">D</option>
                    <option value="E">E</option>
                    <option value="F">F</option>
                </select>
            </div>
        </div>

        <div class="card mb-3 bg-info text-white">
            <div class="card-body">
                <p class="card-text">
                    Your relay team will automatically receive a name based on your club code and age group.
                    You can also give your team an additional fun/team name e.g. "Bob's Awesome Team".
                </p>
            </div>
        </div>

        <div class="form-group row">
            <label for="newTeamName" class="col-sm-2 col-form-label text-right">Team Name: </label>
            <div class="col-sm-10">
                <input type="text" name="newTeamName" id="newTeamName" size="40" class="form-control"/> optional
            </div>
        </div>

        <div class="card mb-3 bg-info text-white">
        <div class="card-body">
            <p class="card-text">
                Only swimmers with at least one individual event entry for this meet will be shown.
            </p>
        </div>
    </div>

    <div class="form-group row">
        <label for="newTeamSwimmer1" class="col-sm-2 col-form-label text-right"
            id="lblSwimmer1">Swimmer 1:</label>
        <div class="col-sm-10">
            <select name="newTeamSwimmer1" id="newTeamSwimmer1" class="newTeamSwimmer form-control">
                <option value=""></option>
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="newTeamSwimmer2" class="col-sm-2 col-form-label text-right"
            id="lblSwimmer2">Swimmer 2:</label>
        <div class="col-sm-10">
            <select name="newTeamSwimmer2" id="newTeamSwimmer2" class="newTeamSwimmer form-control">
                <option value=""></option>
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="newTeamSwimmer3" class="col-sm-2 col-form-label text-right"
            id="lblSwimmer3">Swimmer 3:</label>
        <div class="col-sm-10">
            <select name="newTeamSwimmer3" id="newTeamSwimmer3" class="newTeamSwimmer form-control">
                <option value=""></option>
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="newTeamSwimmer4" class="col-sm-2 col-form-label text-right"
            id="lblSwimmer4">Swimmer 4:</label>
        <div class="col-sm-10">
            <select name="newTeamSwimmer4" id="newTeamSwimmer4" class="newTeamSwimmer form-control">
                <option value=""></option>
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="totalAge" class="col-sm-2 col-form-label text-right">Total Age:</label>
        <div class="col-sm-10">
            <input type="text" name="totalAge" id="totalAge" size="4" disabled/>
        </div>
    </div>

    <div class="card mb-3 bg-warning text-white" id="card-gender-incorrect">
        <div class="card-body">
            <h5 class="card-title">
                Gender incorrect for mixed team!
            </h5>
            <p class="card-text">
                The rules of swimming require a mixed team to consist of two males and two females.
                This team does not contain two male and two female swimmers.
            </p>
            <p class="card-text">
                You are able to still lodge the team, but <strong>you may be disqualified from
                    receiving points, medals or prizes</strong>. Alternately, you can edit the team to have the
                correct number of male and female runners.
            </p>
        </div>
    </div>

    <div class="card mb-3 bg-danger text-white" id="card-age-incorrect">
        <div class="card-body">
            <h5 class="card-title">
                Age incorrect for selected age group
            </h5>
            <p class="card-text">
                You have selected a specific age group for this relay team, however the ages of the
                members chosen does not add up an acceptable total for this age group.
            </p>
            <p class="card-text">
                <strong>Possible Solutions:</strong>
                <ul>
                <li>
                    Change the age group selected
                </li>
                <li>
                    Select different swimmers who's ages will add up to a correct total
                </li>
            </ul>
            </p>
        </div>
    </div>

    <div class="card mb-3 bg-danger text-white" id="card-letter-used">
        <div class="card-body">
            <h5 class="card-title">
                Selected letter already used for this age group
            </h5>
            <p class="card-text">
                You have chosen to specify a letter for your relay team. This letter is already
                being used by another relay team from your club in this age group in this event.
            </p>
            <p class="card-text">
                <strong>Possible Solutions:</strong>
            <ul>
                <li>
                    Delete the existing team using this letter
                </li>
                <li>
                    Select a different letter for this team
                </li>
                <li>
                    Set team letter to "Automatically select"
                </li>
            </ul>
            </p>
        </div>
    </div>

    <div class="card mb-3 bg-info text-white">
        <div class="card-body">
            <p class="card-text">
                Seed time is optional for relay teams. Check the Meet Flyer or with the Meet Director
                to find out if seed times are being used for this meet.
            </p>
            <p class="card-text">
                e.g. enter 5:10.24 or 51024 for 5 minutes 10 seconds and 24 milliseconds
            </p>
        </div>
    </div>

    <div class="form-group row">
        <label for="newTeamSeedTime" class="col-sm-2 col-form-label text-right">Seed time:</label>
        <div class="col-sm-10">
            <input type="text" name="newTeamSeedTime" id="newTeamSeedTime" size="40" class="form-control"/> optional
        </div>
    </div>

    <input type="hidden" name="relayId" id="relayId" value="" />

    <div class="form-group row">
        <input type="button" class="btn btn-primary" id="editTeamSubmit" value="Apply Changes" />
        <input type="button" class="btn btn-primary" id="newTeamCreate" value="Create Team" disabled/>
        <input type="button" class="btn btn-danger" id="resetTeamCreate" value="Reset Form"/>
    </div>

    </div>

</form>

<div id="deleteModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this relay team?</p>
                <p class="text-warning">
                    <small>This can not be undone.</small>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary">Yes, delete the team</button>
            </div>
        </div>
    </div>
</div>

<div class="loading-model"></div>

<script>



</script>

