<?php

// JSON Web Service
// Returns a list of Meets available for viewing via eProgram

require_once("../includes/setup.php");
require_once("../includes/classes/Member.php");

// checkLogin();

// Set up the Associative Array fetch mode
$GLOBALS['db']->setFetchMode(DB_FETCHMODE_ASSOC);

$eventId = intval($_GET['eventId']);

$page_size = intval($_GET['pageSize']);

if ($page_size == 0) {
	$page_size = 8;
}

$page_number = intval($_GET['pageNumber']);

$sort_list = $_GET['orderBy'];
$sort_direction = $_GET['orderDir'];

$limit = $page_size;
$offset = $page_number * $page_size;

if (isset($_GET['nominationSearch'])) {
	$filterSearch = "%" . $_GET['nominationSearch'] . "%";
} else {
	$filterSearch = "%%";
}

if ($eventId != 0) {

// Get list of nominations for relay events
	$nominations = $GLOBALS['db']->getAll( "SELECT a.id,
a.meet_entry_id,
a.event_id,
a.member_id,
m.firstname,
m.surname,
m.number,
m.gender,
a.relay_id,
b.club_id,
c.code,
c.clubname,
a.leg,
a.seedtime,
a.cost,
a.paid
 FROM meet_entries as b, meet_events_entries as a, clubs as c, member as m
WHERE a.event_id = ?
AND a.meet_entry_id = b.id
AND b.club_id = c.id
AND a.member_id = m.id
AND (m.firstname LIKE ? OR m.surname LIKE ?)
ORDER BY m.surname, m.firstname
        LIMIT ? 
        OFFSET ?",
		array( $eventId, $filterSearch, $filterSearch, $limit, $offset ) );
	db_checkerrors( $nominations );

	$nominationsCount = $GLOBALS['db']->getOne( "SELECT count(*)
 FROM meet_entries as b, meet_events_entries as a, clubs as c, member as m
WHERE a.event_id = ?
AND a.meet_entry_id = b.id
AND b.club_id = c.id
AND a.member_id = m.id
AND (m.firstname LIKE ? OR m.surname LIKE ?)", array( $eventId, $filterSearch, $filterSearch ) );
	db_checkerrors( $nominationsCount );

	$nomData = array();

	foreach ( $nominations as $n ) {

		$memberDetails = $GLOBALS['db']->getRow( "SELECT id, firstname, surname, member.number, gender, dob
FROM member WHERE id = ?", array( $n['member_id'] ) );
		db_checkerrors( $memberDetails );

		// Determine age at 31/12 of year of the test date
		$lastDay = date( 'Y' ) . '-12-31';

		$dobDT      = new DateTime( $memberDetails['dob'] );
		$testDateDT = new DateTime( $lastDay );

		$ageInt   = $dobDT->diff( $testDateDT );
		$age      = $ageInt->format( '%y' );
		$n['age'] = $age;

		// Is this nominated person in a team?
		$teamDetails = $GLOBALS['db']->getRow( "SELECT * FROM meet_entries_relays AS a, meet_entries_relays_members AS b 
WHERE a.id = b.relay_team
AND a.meetevent_id = ?
AND b.member_id = ?", array( $eventId, $n['member_id'] ) );
		db_checkerrors( $teamId );

		$n['teamId'] = $teamDetails['id'];


		// Get status
		$entryStatus = $GLOBALS['db']->getOne( "SELECT status FROM meet_events_entries_statuses WHERE meet_event_entries_id = ?
ORDER BY changed DESC LIMIT 1;", array( $n['id'] ) );
		db_checkerrors( $entryStatus );

		$n['statusCode'] = $entryStatus;

		$nomData[] = $n;

	}

// Send JSON Response
	header( 'Content-type: application/json' );
	header( 'Access-Control-Allow-Origin: *' );
	header( 'Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS' );
	header( 'Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization' );

	$callback = filter_input( INPUT_GET, 'callback', FILTER_SANITIZE_SPECIAL_CHARS );

	$response_obj = array(
		'count' => $nominationsCount,
		'data'  => $nomData
	);

} else {
	$response_obj = array(
		'count' => 0,
		'data'  => []
	);
}

if (isset($_GET['callback'])) {

    echo $callback . '(' . json_encode($response_obj) . ');';

} else {

    echo json_encode($response_obj);

}

?>
