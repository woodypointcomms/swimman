<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 31/10/18
 * Time: 9:31 PM
 */

require_once("../includes/setup.php");
require_once("../includes/classes/Meet.php");
require_once("../includes/classes/MeetEvent.php");

// Set up the Associative Array fetch mode
$GLOBALS['db']->setFetchMode(DB_FETCHMODE_ASSOC);

$queryMeet = "SELECT * FROM meet WHERE status = 1 ORDER BY startdate ASC;";
$queryEvents = "SELECT meet_events.id as id,
 meet_events.meet_id as meet_id, 
 meet_events.legs as legs,
 event_distances.distance as distance,
 event_disciplines.discipline as discipline,
  event_types.gender as gender,
  meet_events.eventname as eventname,
  meet_events.prognumber as prognumber,
  meet_events.progsuffix as progsuffix,
  meet_events.eventfee as eventfee,
  meet_events.deadline as deadline 
FROM meet_events, event_disciplines, event_distances, event_types 
WHERE meet_id = ? 
AND meet_events.discipline = event_disciplines.id 
AND meet_events.distance = event_distances.id 
AND meet_events.type = event_types.id ORDER BY prognumber ASC, progsuffix ASC;";

$meets = $GLOBALS['db']->getAll($queryMeet);
db_checkerrors($meets);

$meetData = [];

foreach ($meets as $m) {

	$meetEvents = $GLOBALS['db']->getAll($queryEvents, array($m['id']));
	db_checkerrors($meetEvents);

	$m['events'] = $meetEvents;

	$meetData[] = $m;

}

// Send JSON Response
header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Authorization');
$callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_SPECIAL_CHARS);

if (isset($_GET['callback'])) {

	echo $callback . '(' . json_encode($meetData) . ');';

} else {

	echo json_encode($meetData);

}
