<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 31/10/18
 * Time: 9:31 PM
 */

require_once("../includes/setup.php");
require_once("../includes/classes/Meet.php");
require_once("../includes/classes/MeetEvent.php");

// Set up the Associative Array fetch mode
$GLOBALS['db']->setFetchMode(DB_FETCHMODE_ASSOC);

$memberId = intval($_GET['member_id']);

$queryAccess = "SELECT club_id, code, clubname, rolename FROM clubs, club_roles, club_role_types 
WHERE club_roles.member_id = ?
AND clubs.id = club_roles.club_id
AND club_role_types.id = club_roles.role_id;";

$clubAccess = $GLOBALS['db']->getAll($queryAccess, array($memberId));
db_checkerrors($clubAccess);

// Send JSON Response
header('Content-type: application/json');
$callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_SPECIAL_CHARS);

if (isset($_GET['callback'])) {

	echo $callback . '(' . json_encode($clubAccess) . ');';

} else {

	echo json_encode($clubAccess);

}
