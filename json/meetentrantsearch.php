<?php

// JSON Web Service
// Returns a list of Meets available for viewing via eProgram

require_once("../includes/setup.php");
require_once("../includes/classes/Member.php");

header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');

// checkLogin();

// Set up the Associative Array fetch mode
$GLOBALS['db']->setFetchMode(DB_FETCHMODE_ASSOC);

$meetId = intval($_GET['meetId']);

if (isset($_GET['entrantSearch'])) {
	$filterSearch = "%" . $_GET['entrantSearch'] . "%";
} else {
	$filterSearch = "%%";
}

// Get list of nominations for relay events
$entrants = $GLOBALS['db']->getAll("SELECT member.id, member.firstname, member.surname, member.dob
FROM meet_entries, member 
WHERE member.id = meet_entries.member_id
AND meet_entries.meet_id = ?
AND (member.firstname LIKE ? OR member.surname LIKE ?);",
    array($meetId, $filterSearch, $filterSearch));
db_checkerrors($entrants);

// Send JSON Response


$callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_SPECIAL_CHARS);


if (isset($_GET['callback'])) {

    echo $callback . '(' . json_encode($entrants) . ');';

} else {

    echo json_encode($entrants);

}

?>
