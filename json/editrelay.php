<?php

// JSON Web Service
// Returns a list of Meets available for viewing via eProgram

require_once("../includes/setup.php");
require_once("../includes/classes/Meet.php");
require_once("../includes/classes/Club.php");
require_once("../includes/classes/Member.php");
require_once("../includes/classes/RelayEntry.php");
require_once("../includes/classes/RelayEntryMember.php");

// checkLogin();

// Set up the Associative Array fetch mode
//$GLOBALS['db']->setFetchMode(DB_FETCHMODE_ASSOC);

$relayId = $_POST['relayId'];
$newTeamClub = $_POST['newTeamClub'];
$newTeamName = $_POST['newTeamName'];
$newTeamLetter = $_POST['newTeamLetter'];
$newTeamSwimmer1 = $_POST['newTeamSwimmer1'];
$newTeamSwimmer2 = $_POST['newTeamSwimmer2'];
$newTeamSwimmer3 = $_POST['newTeamSwimmer3'];
$newTeamSwimmer4 = $_POST['newTeamSwimmer4'];
$newTeamSeedTime = $_POST['newTeamSeedTime'];

addlog("editrelay.php", "Received Relay Entry edit message for relay $relayId");

$relayEntry = new RelayEntry();
$relayEntry->load($relayId);

$relayEntry->setTeamName($newTeamName);

$relayEntry->deleteMembers();

$relayEntry->addMember(1, $newTeamSwimmer1);
$relayEntry->addMember(2, $newTeamSwimmer2);
$relayEntry->addMember(3, $newTeamSwimmer3);
$relayEntry->addMember(4, $newTeamSwimmer4);

$relayEntry->storeMembers();

$relayEntry->calcAgeGroup();

if (empty($newTeamLetter)) {
	$relayEntry->getNextLetter();
} else {

	// Is there an existing relay with this letter?
	$relayEntry->setLetter($newTeamLetter);

}

$relayEntry->setSeedTime(sw_timeToSecs($newTeamSeedTime));


//print_r($relayEntry);

$relayEntry->update();

// Send JSON Response
header('Content-type: application/json');
$callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_SPECIAL_CHARS);

if (isset($_GET['callback'])) {

    echo $callback . '(' . json_encode(true) . ');';

} else {

    echo json_encode(true);

}

?>
