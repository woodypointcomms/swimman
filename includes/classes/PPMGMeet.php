<?php
require_once("includes/setup.php");
require_once("includes/classes/Meet.php");
require_once("includes/classes/MeetEvent.php");
require_once("includes/classes/MeetEntry.php");
require_once("includes/classes/MeetEntryEvent.php");
require_once("includes/classes/PPMGEntry.php");
require_once("includes/classes/PPMGMeetEvent.php");
require_once("includes/classes/PPMGEntryEvent.php");

require_once ($_SERVER['DOCUMENT_ROOT'] . "/swimman/vendor/autoload.php");

/**
 * Created by PhpStorm.
 * User: david
 * Date: 11/10/2016
 * Time: 7:07 AM
 */
class PPMGMeet
{

    private $year;      // PPMG Meet Year
    private $meetId;    // Swimman Meet Id
    private $datafile;  // PPMG Data File

    private $meetObj;

    private $events;    // Array of PPMGMeetEvents
    private $entries;   // Array of PPMGEntries


    function load($year) {

        $ppmgMeet = $GLOBALS['db']->getRow("SELECT * FROM PPMG_meets WHERE meet_year = ?", array($year));
        db_checkerrors($ppmgMeet);

        $this->year = $ppmgMeet[0];
        $this->meetId = $ppmgMeet[1];
        $this->datafile = $ppmgMeet[2];

        // Get an Entry Manager meet object
        $this->meetObj = new Meet();
        $this->meetObj->loadMeet($this->meetId);

        $ppmgMeetEvents = $GLOBALS['db']->getAll("SELECT * FROM PPMG_meetevent WHERE meet_year = ?",
            array($year));
        db_checkerrors($ppmgMeetEvents);

        foreach ($ppmgMeetEvents as $e) {

            $this->events[] = new PPMGMeetEvent($e[1], $e[2], $e[3], $e[4], $e[5]);

        }

    }

    /**
     * Loads the PPMG Data File, finds the event columns and matches them
     * to entry manager events for this meet
     */
    function matchEvents() {

        // Open Datafile CSV
        $uploaddir = $GLOBALS['home_dir'] . '/../masters-tmp/';
        $csvFile = fopen ( $uploaddir . $this->datafile, "r" );

        // Find columns
        $titleLine[] = array();
        while(count($titleLine) <= 1) {

            $titleLine = fgetcsv ( $csvFile );

        }

        // Set through the titleline to find
        $colNo = 0;
        foreach ($titleLine as $t) {

            // Match event selection columns
            if (!preg_match('/Nominated Time/i', $t)) {

                // Look for swimming event name
                if (preg_match('/Freestyle/i', $t)) {
                    $stroke = 1;    // Event Manager Freestyle
                }

                if (preg_match('/Breaststroke/i', $t)) {
                    $stroke = 2;    // Event Manager Breaststroke
                }

                if (preg_match('/Butterfly/i', $t)) {
                    $stroke = 3;    // Event Manager Butterfly
                }

                if (preg_match('/Backstroke/i', $t)) {
                    $stroke = 4;    // Event Manager Backstroke
                }

                if (preg_match('/Individual Medley/i', $t)) {
                    $stroke = 5;    // Event Manager Individual Medley
                } else if (preg_match('/Medley/i', $t)) {
		            $stroke = 6;    // Event Manager Individual Medley
	            }

                // Get distance
                if (preg_match('/\d{2,4}m/', $t)) {

                    preg_match('/\d{2,4}/', $t, $distanceArr);
                    $distance = $distanceArr[0];

                    if (preg_match('/4 x/', $t)) {
                    	$legs = 4;
	                } else {
                    	$legs = 1;
                    }

                    if (preg_match('/Male/', $t)) {
                    	$gender = 1;
                    } else if (preg_match('/Female/', $t)) {
                    	$gender = 2;
                    } else {
                    	$gender = 3;
                    }

                    // Found an event so now search through this meet's events
                    // to find a matching event;
                    $emEvents = $this->meetObj->getEventList();

                    foreach ($emEvents as $e) {

                        // Load the meet event
                        $event = new MeetEvent();
                        $event->load($e);

                        // Exclude relays from matching
                        // if ($event->getLegs() == 1) {

	                    // print("$distance $stroke Attempting to match event " . $event->getShortDetails() . "<br />");

                        if (($event->getDistanceMetres() == $distance) &&
                            ($event->getDiscipline() == $stroke) &&
                            ($event->getLegs() == $legs) &&
                            ($event->getGender() == $gender)) {

                            // We have a match, store it
                            $ppmgMeetEvent = new PPMGMeetEvent($this->year, $this->meetId, $e, $t, $colNo);
                            $ppmgMeetEvent->store();

                        }

                        // }

                    }

                }
            }

            $colNo++;
        }

        fclose($csvFile);

    }

    /**
     * Loads the PPMG Data File and Creates PPMGEntry Objects
     */
    function matchMembers() {

        // Open Datafile CSV
        $uploaddir = $GLOBALS['home_dir'] . '/../masters-tmp/';

	    /* Map Rows and Loop Through Them */
	    $rows   = array_map('str_getcsv', file($uploaddir . $this->datafile));
	    $header = array_shift($rows);
	    $csvData   = array();
	    foreach($rows as $row) {
		    $csvData[] = array_combine($header, $row);
	    }

	    # $csvData = \Rap2hpoutre\Csv\csv_to_associative_array($uploaddir . $this->datafile);

        // Step through all records
        foreach ($csvData as $entryData) {

            $entry = new PPMGEntry();

            // Check entry doesn't already exist
            if (!$entry->load($entryData['id'])) {

            	$entry->setMeetYear("2018");
                $entry->setAccountNumber($entryData['id']);
                $entry->setDateRegistered($entryData['dateofentry']);
                $entry->setRecordType($entryData['sportname']);
                $entry->setFirstName($entryData['firstname']);
                $entry->setLastName($entryData['lastname']);
                $entry->setGender($entryData['gender']);
                $entry->setMainCountry($entryData['country']);
                $entry->setDateOfBirth($entryData['dateofbirth']);
                $entry->setAge($entryData['age']);
                $entry->setPrimaryContactNumber('phone');
                $entry->setEmail($entryData['email']);
                $entry->setMainState($entryData['state']);
                $entry->setEmergencyContactName($entryData['emergencycontact']);
                $entry->setEmergencyContactPhoneNumber($entryData['emergencyphone']);
                $entry->setAgeGroup($entryData['age-group']);
                $entry->setMsaMember($entryData['AusMastersMember']);
                $entry->setMsaId($entryData['ID']);
                $entry->setMsaClubCode($entryData['ClubCode']);
	            $entry->setNonAustralianMasterMember($entryData['NonMember']);
                $entry->setOverseasMastersSwimmingMember($entryData['OverseasMastersMem']);
                $entry->setOverseasMastersSwimmingCountry($entryData['Country']);
                $entry->setOverseasMastersSwimmingClubName($entryData['ClubName']);
                $entry->setDisability($entryData['Disability']);
	            $entry->setMulticlass($entryData['MultiClass']);
	            $entry->setClassification($entryData['Classification']);

                // $entry->findEntryManagerMember();

                $entry->store();

            } else {

                // Entry already exists

                // Match up member
                $entry->findEntryManagerMember();

                $entry->updateMemberEntry();

            }

        }

    }

    public function createEntries() {

        // Open Datafile CSV
        $uploaddir = $GLOBALS['home_dir'] . '/../masters-tmp/';
        $csvFile = fopen ( $uploaddir . $this->datafile, "r" );

        // Get title line as discard it
        $titleLine = fgetcsv ( $csvFile );

        // Step through all records
        while ($entryData = fgetcsv($csvFile)) {

            $entry = new PPMGEntry();

            print("$entryData[2]<br />");

            // Check entry doesn't already exist
            if ($entry->load($entryData[2])) {

                // Check we have an member link
                if ($entry->getMemberId() != "") {

                    $clubCode = '';

                    if ($entry->getMsaClubCode() != "") {
                        $clubCode = $entry->getMsaClubCode();
                    } elseif ($entry->getOverseasMastersSwimmingClubCode() != "") {
                        $clubCode = $entry->getOverseasMastersSwimmingClubCode();
                    } else {
                        $clubCode = "UNAT";
                    }

                    $club = new Club();
                    $club->load($clubCode);

                    $meetEntry = new MeetEntry($entry->getMemberId(), $club->getId(), $this->meetId);

                    // Step through the events
                    foreach ($this->events as $e) {

                        $selectionCol = $e->getPPMGcolumn();
                        // $timeCol = $e->getPPMGcolumn() + 1;

                        if (! empty($entryData[$selectionCol])) {

                            // Entry includes this event
                            $meetEntry->addEvent($e->getMeetEventId(), sw_timeToSecs($entryData[$selectionCol]));

                            // Store the PPMG event
                            $ppmgEntryEvent = new PPMGEntryEvent($entry->getAccountNumber(),
                                $selectionCol, $entryData[$selectionCol]);

                            $ppmgEntryEvent->store();


                        }

                    }

                    $existEntry = new MeetEntry($entry->getMemberId(), $club->getId(), $this->meetId);

                    if (!$existEntry->load()) {

                        // Create the entry
                        $meetEntry->create();
                        $emEntryId = $meetEntry->getId();

                        // Store the entry and event entry id
                        $entry->setEntryId($emEntryId);
                        $entry->updateEntryId($this->meetId);

                        // Update status
                        // $meetEntry->setStatus(2);
                        // $meetEntry->setEventStatuses(2);

                        $meetEntry->updateStatus();
                        $meetEntry->updateEventStatuses();

                    }

                } else {
                	print("No member link<br />");
                }


            } else {
            	print("PPMG Entry not found<br />");
            }

        }

        fclose($csvFile);

    }
}