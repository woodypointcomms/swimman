import {Component} from '@angular/core';
import {AuthenticationService} from "./services/authentication-service.service";

@Component({
    selector: 'app-root',
    styleUrls: [
        './app.component.css'
    ],
    templateUrl: './app.component.html',
})
export class AppComponent {
    title = 'ng-backend';

    constructor(private authService: AuthenticationService) {

    }
}
