/**
 * An object used to hold Meet Information
 */
import {MeetEvent} from "./meetevent";

export class Meet {
    id: Number;
    meetname: String;
    startdate: Date;
    enddate: Date;
    deadline: Date;
    contactname: String;
    contactemail: Number;
    contactphone: Number;
    meetfee: Number;
    mealfee: Number;
    location: String;
    status: Number;
    maxevents: Number;
    mealsincluded: Number;
    mealname: String;
    massagefee: Number;
    programfee: Number;

    events: MeetEvent[];

}