import {Club} from "src/app/models/club";

/**
 * An object used to hold Joomla User Information
 */
export class JoomlaUser {
    jId: number = 0;
    jUsername: string;
    jName: string;
    jMsaNumber: string;
    jClub: string;
    jDob: string;
    jRegisterDate: string;
    jLastVisitDate: string;
    mId: number = 0;
    mFirstName: string;
    mSurname: string;
    mMsaNumber: string;
    mDob: string;
    mClubs = new Array<Club>();
    linked: boolean;
    canBeAutoLinked: boolean;
}
