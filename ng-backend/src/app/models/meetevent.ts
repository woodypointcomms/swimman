/**
 * An object used to hold Meet Event Information
 */
export class MeetEvent {

    id: Number;
    meet_id: Number;
    legs: Number;
    distance: String;
    discipline: String;
    gender: Number;
    eventname: String;
    prognumber: Number;
    progsuffix: String;
    eventfee: Number;
    deadline: Date;

}