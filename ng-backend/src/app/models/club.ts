/**
 * An object used to hold Club Information
 */
export class Club {

    id: number;
    code: string;
    clubName: string;
    clubNameShort: string;

}