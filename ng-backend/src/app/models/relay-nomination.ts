/**
 * An object used to hold Relay Nominations
 */
export class RelayNomination {
    meet_entry_id;
    event_id;
    member_id;
    relay_id;
    club_id;
    code;
    clubname;
    leg;
    seedtime;
    cost;
    paid;
    firstname;
    surname;
    number;
    gender;
    age;
    teamId;
    status;
}
