import {Component, Input, OnInit} from '@angular/core';
import { JoomlaUser } from "../models/joomla-user";
import { NgbActiveModal, NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-joomla-user-link-modal',
  templateUrl: './joomla-user-link-modal.component.html',
  styleUrls: ['./joomla-user-link-modal.component.css']
})

export class JoomlaUserLinkModalComponent implements OnInit {
    @Input() userdata: JoomlaUser;

    constructor(public activeModal: NgbActiveModal) {
    }

    ngOnInit() {
        console.log(this.userdata);
    }

    getDate(dateString: string) {
        if (dateString != "" && dateString != null) {
            var dateComps = dateString.split('-');
            return new Date(parseInt(dateComps[0]), parseInt(dateComps[1]), parseInt(dateComps[2]));
        } else {
            return null;
        }
    }

    onCancelClick() {
        console.log("User link/unlink cancelled");
        this.activeModal.dismiss();
    }

    onLinkClick() {
        console.log("Link requested");
        this.activeModal.close();
    }

    onUnlinkClick() {
        console.log("Unlink requested");
        this.activeModal.close();
    }

}
