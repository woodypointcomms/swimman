import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

import {AppComponent} from './app.component';
import {JoomlaUserListComponent} from './joomla-user-list/joomla-user-list.component';
import {HttpClientModule} from '@angular/common/http';
import {JoomlaUserLinkModalComponent} from "./joomla-user-link-modal/joomla-user-link-modal.component";
import {ReactiveFormsModule} from "@angular/forms";
import {NgxSpinnerModule} from "ngx-spinner";
import {LoginComponent} from './login/login.component';
import {AppRoutingModule} from './app-routing.module';
import {AuthenticationModule} from './authentication.module';
import {HeaderComponent} from './header/header.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {JoomlaUserEditComponent} from './joomla-user-edit/joomla-user-edit.component';
import {ImportMeetEntriesComponent} from './import-meet-entries/import-meet-entries.component';
import {UploadModule} from "./upload/upload.module";
import {RelayNominationComponent} from './relay-nomination/relay-nomination.component';
import {EntrantService} from "./services/entrant.service";
import {MeetService} from "./services/meet.service";

@NgModule({
    declarations: [
        AppComponent,
        JoomlaUserListComponent,
        JoomlaUserLinkModalComponent,
        LoginComponent,
        HeaderComponent,
        SidebarComponent,
        JoomlaUserEditComponent,
        ImportMeetEntriesComponent,
        RelayNominationComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        NgxDatatableModule,
        NgbModule,
        FontAwesomeModule,
        ReactiveFormsModule,
        NgxSpinnerModule,
        AppRoutingModule,
        AuthenticationModule,
        UploadModule
    ],
    entryComponents: [
        JoomlaUserLinkModalComponent
    ],
    providers: [HttpClientModule,
        MeetService,
        EntrantService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
