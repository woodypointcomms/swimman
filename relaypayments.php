<?php
require_once("includes/setup.php");
require_once("includes/sidebar.php");
require_once("includes/classes/Club.php");
require_once("includes/classes/Member.php");
require_once("includes/classes/Meet.php");
require_once("includes/classes/MeetEvent.php");
require_once("includes/classes/MeetSelector.php");
checkLogin();

if (isset($_POST['submitrelaypayment'])) {

    $clubId = intval($_POST['club']);
	$numRelays = intval($_POST['numrelays']);
	$method = intval($_POST['method']);

	$insert = $GLOBALS['db']->query("INSERT INTO relay_payments (meet_id, club_id, qty, amount, datetime, method)
                VALUES (?, ?, ?, ?, now(), ?)",
        array(158, $clubId, $numRelays, ($numRelays * 12), $method));
	db_checkerrors($insert);

}

htmlHeaders("Swimming Management System - Relay Payments");

sidebarMenu();

echo "<div id=\"main\">\n";

?>

<h2>Relay Payments</h2>

<form method="post">

    <table width="100%" class="list" id="data" >
        <thead class="list">
        <tr id="paymentHeader">
            <th>Club</th>
            <th style='text-align: center;'>Event #3</th>
            <th style='text-align: center;'>Event #10</th>
            <th style='text-align: center;'>Event #15</th>
            <th style='text-align: center;'>Event #16</th>
            <th style='text-align: center;'>Event #21</th>
            <th style='text-align: center;'>Event #22</th>
            <th style='text-align: center;'>Teams</th>
            <th>Owed</th>
            <th>Paid</th>
        </tr>
        </thead>
        <tbody class="list" id="relayPayments">

        <?php

            $clubs = $GLOBALS['db']->getAll("SELECT * FROM meet_entries_relays WHERE meet_id = ?
                GROUP BY club_id;",
                array(112));
            db_checkerrors($clubs);

            $event3total = 0;
            $event10total = 0;
            $event15total = 0;
            $event16total = 0;
            $event21total = 0;
            $event22total = 0;
            $totaltotal = 0;
            $totalpayments = 0;

            foreach ($clubs as $c) {

                $clubDetails = new Club();
                $clubDetails->load($c[2]);

                $clubId = $c[2];
                $clubCode = $clubDetails->getCode();
	            $clubName = $clubDetails->getName();

	            $clubTotal = 0;

                echo "<tr>\n";

                echo "<td>\n";
                echo "<abbr title=\"$clubName\">$clubCode</abbr>\n";
                echo "</td>\n";

                // Get event 3
                $event3 = $GLOBALS['db']->getOne("SELECT count(*) FROM meet_entries_relays 
                    WHERE meet_id = ? AND club_id = ? AND meetevent_id = ?;",
                    array(112, $clubId, 2223));

	            $event3total += $event3;
	            $clubTotal += $event3;

                echo "<td style='text-align: center;'>\n";
                echo $event3;
	            echo "</td>\n";

	            // Get event 10
	            $event10 = $GLOBALS['db']->getOne("SELECT count(*) FROM meet_entries_relays 
                    WHERE meet_id = ? AND club_id = ? AND meetevent_id = ?;",
		            array(112, $clubId, 2230));

	            $event10total += $event10;
	            $clubTotal += $event10;

	            echo "<td style='text-align: center;'>\n";
	            echo $event10;
	            echo "</td>\n";

	            // Get event 15
	            $event15 = $GLOBALS['db']->getOne("SELECT count(*) FROM meet_entries_relays 
                    WHERE meet_id = ? AND club_id = ? AND meetevent_id = ?;",
		            array(112, $clubId, 2235));

	            $event15total += $event15;
	            $clubTotal += $event15;

	            echo "<td style='text-align: center;'>\n";
	            echo $event15;
	            echo "</td>\n";

	            // Get event 16
	            $event16 = $GLOBALS['db']->getOne("SELECT count(*) FROM meet_entries_relays 
                    WHERE meet_id = ? AND club_id = ? AND meetevent_id = ?;",
		            array(112, $clubId, 2236));

	            $event16total += $event16;
	            $clubTotal += $event16;

	            echo "<td style='text-align: center;'>\n";
	            echo $event16;
	            echo "</td>\n";

	            // Get event 21
	            $event21 = $GLOBALS['db']->getOne("SELECT count(*) FROM meet_entries_relays 
                    WHERE meet_id = ? AND club_id = ? AND meetevent_id = ?;",
		            array(112, $clubId, 2241));

	            $event21total += $event21;
	            $clubTotal += $event21;

	            echo "<td style='text-align: center;'>\n";
	            echo $event21;
	            echo "</td>\n";

	            // Get event 22
	            $event22 = $GLOBALS['db']->getOne("SELECT count(*) FROM meet_entries_relays 
                    WHERE meet_id = ? AND club_id = ? AND meetevent_id = ?;",
		            array(112, $clubId, 2242));

	            $event22total += $event22;
	            $clubTotal += $event22;

	            echo "<td style='text-align: center;'>\n";
	            echo $event22;
	            echo "</td>\n";

	            echo "<td style='text-align: center;'>\n";
	            echo $clubTotal;
	            echo "</td>\n";

	            echo "<td style='text-align: right;'>$\n";
	            echo number_format(($clubTotal * 20), 2);
	            echo "</td>\n";

	            echo "<td style='text-align: right;'>\n";

	            $payments = $GLOBALS['db']->getOne("SELECT sum(amount) FROM relay_payments 
                      WHERE meet_id = ? AND club_id = ?;",
                    array(112, $clubId));
	            db_checkerrors($payments);

	            $totalpayments += $payments;

	            echo "$" . number_format($payments, 2);

	            echo "</td>\n";

	            echo "</tr>\n";

                $totaltotal += $clubTotal;

            }

            echo "</tbody>\n";

            echo "<tfoot>\n";

            echo "<tr>\n";

            echo "<td>\n";
        echo "Total\n";
            echo "</td>\n";

            echo "<td style='text-align: center;'>\n";
            echo $event3total;
            echo "</td>\n";

        echo "<td style='text-align: center;'>\n";
        echo $event10total;
        echo "</td>\n";

        echo "<td style='text-align: center;'>\n";
        echo $event15total;
        echo "</td>\n";

        echo "<td style='text-align: center;'>\n";
        echo $event16total;
        echo "</td>\n";

        echo "<td style='text-align: center;'>\n";
        echo $event21total;
        echo "</td>\n";

        echo "<td style='text-align: center;'>\n";
        echo $event22total;
        echo "</td>\n";

        echo "<td style='text-align: center;'>\n";
        echo $totaltotal;
        echo "</td>\n";

        echo "<td style='text-align: right;'>$\n";
        echo number_format(($totaltotal * 20), 2);
        echo "</td>\n";

        echo "<td style='text-align: right;'>$\n";
        echo number_format(($totalpayments), 2);
        echo "</td>\n";

        echo "</tr>\n";
        echo "<tfoot>\n";

        ?>

    </table>

    <h2>Enter Payment</h2>
    <fieldset>
        <p>
            <label for="club">Club: </label>
            <select name="club" id="club">
                <option></option>
            </select>
        </p>
        <p>
            <label for="numrelays">Qty: </label>
            <input type="number" name="numrelays" id="numrelays" />
        </p>
        <p>
            <label for="method">Method: </label>
            <select name="method" id="method">
                <option value="2">PayPal</option>
                <option value="1">Cash</option>
            </select>
        </p>
        <p>
            <input type="submit" name="submitrelaypayment" value="Submit Payment" />
            <input type="reset" name="restrelaypayment" value="Reset" />
        </p>

    </fieldset>

</form>

<script>

    meetId = 112;

    $( document ).ready(function() {

//        var url = "/swimman/json/relayevents.php?meetId=" + meetId;
//
//        var relayEvents = new Array();
//        var teams = new Array();
//
//        $.getJSON(url, {
//            format: "json"
//        })
//            .done(function (data) {
//
//                $('select[name=eventId]')
//                    .find('option')
//                    .remove();
//
//                $.each(data, function (key, value) {
//
//                    // Populate the events drop down
//                    var eventId = value.id;
//                    var eventDesc = "#" + value.prognumber + value.progsuffix;
//                    var eventArray = [eventId, eventDesc];
//
//                    relayEvents.push(eventArray);
//
//                })
//
//                for (var i = 0; i < relayEvents.length; i++) {
//
//                    $('#paymentHeader').append("<th>Event " + relayEvents[i][1] + "</th>");
//
//
//
//                }
//
//                $('#paymentHeader').append("<th>Teams</th>");
//                $('#paymentHeader').append("<th>Owed</th>");
//                $('#paymentHeader').append("<th>Paid</th>");
//
//
//
//            });




        $('#club').combobox();

        $('#data').DataTable();

        var url2 = "/swimman/json/relayclubs.php?meetId=" + meetId;

        $.getJSON(url2, {
            format: "json"
        })
            .done(function (data) {

                $('#club')
                    .find('option')
                    .remove();

                $('#club')
                    .append('<option value="0">Unattached</option>');

                $.each(data, function (key, value) {

                    // Populate the events drop down
                    var clubId = value.id;
                    var clubName = value.clubname;
                    var clubCode = value.code;

                    $('#club')
                        .append('<option value="' + clubId + '">' + clubName + ' (' + clubCode + ')</option>');

                });

                $('#club').combobox("resize");

            });



    });

</script>

<?php 

echo "</div>\n"; // main div

htmlFooters();


?>