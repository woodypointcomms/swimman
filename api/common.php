<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 23/12/17
 * Time: 1:16 PM
 */

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');

require_once ($_SERVER['DOCUMENT_ROOT'] . "/swimman/vendor/autoload.php");

// Authentication logger
$log_level = Monolog\Logger::DEBUG;
$GLOBALS['apiLog'] = new Monolog\Logger('member');
$GLOBALS['apiLog']->pushProcessor(new \Monolog\Processor\WebProcessor);
$GLOBALS['apiLog']->pushHandler(new \Monolog\Handler\StreamHandler($_SERVER['DOCUMENT_ROOT']
                                                                   . "/logs/" . 'api.log', $log_level));

function check_authorisation($token) {

	try {

		$key = "example_key";
		$authData = JWT::decode($token, $key, array('HS256'));
		return True;

	} catch (Exception $e) {

		return False;

	}

}

function is_user_logged_in() {

	$headers = getallheaders();

	if (isset($headers['Authorization'])) {
		$token = str_replace('Bearer ', '', $headers['Authorization']);
	} else {
		return False;
	}

	if (check_authorisation($token)) {
		return True;
	} else {
		return False;
	}

}

function getPDO() {

	$host = 'mysql1.quadrahosting.com';
	$port = 3306;
	$db   = 'davsoft_msqold';
	$user = 'davsoft_data';
	$pass = 'davez1';
	$charset = 'utf8mb4';

	$dsn = "mysql:host=$host;port=$port;dbname=$db;charset=$charset";
	$opt = [
		PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
		PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
		PDO::ATTR_EMULATE_PREPARES   => false,
	];

	$pdo = new PDO($dsn, $user, $pass, $opt);

	return $pdo;
}