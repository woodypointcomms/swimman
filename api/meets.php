<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 23/12/17
 * Time: 12:08 PM
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/swimman/vendor/autoload.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/swimman/api/common.php");

use \Firebase\JWT\JWT;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// Authentication logger
$log_level = Logger::DEBUG;
$apiLog = new Logger('meets');
$apiLog->pushProcessor(new \Monolog\Processor\WebProcessor);
$apiLog->pushHandler(new StreamHandler($_SERVER['DOCUMENT_ROOT'] . "/logs/" . 'api.log', $log_level));

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	// return only the headers and not the content
	// only allow CORS if we're doing a GET - i.e. no saving for now.
	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']) &&
	    $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'] == 'GET') {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Authorization');
	}
	exit;
}

$headers = getallheaders();

print_r($headers);

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Authorization');


// Check if user has authorization, if they do, may be able to give more data
$authData;
if (isset($headers['Authorization'])) {

	print("auth attempt " . $headers['Authorization']);

	$token = str_replace('Bearer ', '', $headers['Authorization']);

	try {

		$key = "example_key";
		$authData = JWT::decode($token, $key, array('HS256'));

		print($authData);

	} catch (Exception $e) {

		$apiLog->error("Invalid token in Meets service");


	}

}

// Default to last year, this year and next year
$year = date("Y",strtotime("-1 year"));
$startdate = $year . "-01-01";

$pdo = getPDO();
$meets = array();

$statement = $pdo->prepare("SELECT m.id, 
	m.meetname,
	m.startdate,
	m.enddate,
	m.deadline,
	m.contactname,
	p.phonenumber as contactphone,
	e.address as contactemail,
	m.meetfee,
	m.mealfee,
	m.location,
	m.status,
	m.maxevents,
	m.mealsincluded,
	m.mealname,
	m.massagefee,
	m.programfee	 
	FROM meet as m, phones as p, emails as e 
	WHERE startdate >= :startdate
	AND m.contactphone = p.id
	AND m.contactemail = e.id;");
if ($statement->execute([ ":startdate" => $startdate ])) {

	while ( $meetDetails = $statement->fetch( PDO::FETCH_ASSOC ) ) {

		$meet_id = $meetDetails['id'];

		// Get events for this meet
		$stmt = $pdo->prepare("SELECT 
			a.id, 
			b.typename,
			b.relay,
			CASE b.gender 
				WHEN 1 THEN 'M'
				WHEN 2 THEN 'F'
				WHEN 3 THEN 'X'
			END as gender,
			c.discipline,
			c.abrev as discipline_short,
			d.distance,
			d.metres as metres,
			d.course as course,
			a.eventname,
			a.prognumber,
			a.progsuffix,
			a.eventfee,
			a.deadline  
			FROM meet_events as a, event_types as b, event_disciplines as c, event_distances as d
			WHERE a.meet_id = :meet_id 
			AND a.type = b.id
			AND a.discipline = c.id
			AND a.distance = d.id
			ORDER BY a.prognumber, a.progsuffix;");
		$stmt->execute([ ":meet_id" => $meet_id]);

		$events = array();

		while ($eventDetails = $stmt->fetch(PDO::FETCH_ASSOC)) {

			$events[] = $eventDetails;

		}

		$meetDetails['events'] = $events;
		$meets[] = $meetDetails;

	}

}

$meets_json = json_encode($meets);

header('Content-type: application/json');
echo $meets_json;

$apiLog->info("Served meet data");
