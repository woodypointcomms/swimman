<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . "/swimman/vendor/autoload.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/swimman/api/common.php");

use \Firebase\JWT\JWT;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// Check if user has authorization, if they do, may be able to give more data
$authData;
if (isset($headers['Authorization'])) {

	print("auth attempt " . $headers['Authorization']);

	$token = str_replace('Bearer ', '', $headers['Authorization']);

	try {

		$key = "example_key";
		$authData = JWT::decode($token, $key, array('HS256'));

		print($authData);

	} catch (Exception $e) {

		$apiLog->error("Invalid token in juserlist service");
		header( 'HTTP/1.1 401 Unauthorized', true, 401 );
		exit();

	}

}

// JSON Web Service
// Returns a list of Joomla Users

require_once( "../includes/setup.php" );
require_once( "../includes/classes/Member.php" );
require_once( "../includes/classes/Club.php" );

ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);

// Set up the Associative Array fetch mode
$GLOBALS['db']->setFetchMode(DB_FETCHMODE_ASSOC);

$page_size = intval($_GET['pageSize']);

if ($page_size == 0) {
    $page_size = 8;
}

$page_number = intval($_GET['pageNumber']);

$sort_list = $_GET['orderBy'];
$sort_direction = $_GET['orderDir'];

$limit = $page_size;
$offset = $page_number * $page_size;

if ($sort_list == "jRegisterDate") {
    $order_by = "j_users.registerDate";
} else {
	$order_by = "j_users.registerDate";
}

if ($sort_direction == "desc") {
	$order_by .= " DESC";
} else {
	$order_by .= " ASC";
}

if (isset($_GET['filterSearch'])) {
	$filterSearch = "%" . $_GET['filterSearch'] . "%";
} else {
    $filterSearch = "%%";
}

if ($_GET['filterOption'] == 'registered7days') {

	$sql = "SELECT DISTINCT j_users.id as jId, 
               j_users.name as jName, 
               j_users.username as jUsername, 
               j_users.email as jEmail, 
               j_users.registerDate as jRegisterDate, 
               j_users.lastvisitDate as jLastVisitDate, 
               member_msqsite.member_id as mId, 
               member.firstname as mFirstName, 
               member.surname as mSurname, 
               member.number as mMsaNumber,
               member.dob as mDob
        FROM j_users
        LEFT JOIN member_msqsite ON j_users.id = member_msqsite.joomla_uid
        LEFT JOIN member ON member_msqsite.member_id = member.id
        WHERE j_users.registerDate > (CURDATE() - INTERVAL 7 DAY)
        AND (j_users.username LIKE ? OR j_users.name LIKE ?)
        ORDER BY $order_by
        LIMIT ? 
        OFFSET ?";

	$sql_count = "SELECT DISTINCT count(j_users.id)
        FROM j_users
        LEFT JOIN member_msqsite ON j_users.id = member_msqsite.joomla_uid
        LEFT JOIN member ON member_msqsite.member_id = member.id
        WHERE j_users.registerDate > (CURDATE() - INTERVAL 7 DAY)
        AND (j_users.username LIKE ? OR j_users.name LIKE ?)";

} elseif ($_GET['filterOption'] == 'unlinked') {

	$sql = "SELECT DISTINCT j_users.id as jId, 
               j_users.name as jName, 
               j_users.username as jUsername, 
               j_users.email as jEmail, 
               j_users.registerDate as jRegisterDate, 
               j_users.lastvisitDate as jLastVisitDate, 
               member_msqsite.member_id as mId, 
               member.firstname as mFirstName, 
               member.surname as mSurname, 
               member.number as mMsaNumber,
               member.dob as mDob
        FROM j_users
        LEFT JOIN member_msqsite ON j_users.id = member_msqsite.joomla_uid
        LEFT JOIN member ON member_msqsite.member_id = member.id
        WHERE member_msqsite.member_id IS NULL
        AND (j_users.username LIKE ? OR j_users.name LIKE ?)
        ORDER BY $order_by
        LIMIT ? 
        OFFSET ?";

	$sql_count = "SELECT DISTINCT count(j_users.id)
        FROM j_users
        LEFT JOIN member_msqsite ON j_users.id = member_msqsite.joomla_uid
        LEFT JOIN member ON member_msqsite.member_id = member.id
        WHERE member_msqsite.member_id IS NULL
        AND (j_users.username LIKE ? OR j_users.name LIKE ?);";

} elseif ($_GET['filterOption'] == 'linked') {

    $sql = "SELECT DISTINCT j_users.id as jId, 
               j_users.name as jName, 
               j_users.username as jUsername, 
               j_users.email as jEmail, 
               j_users.registerDate as jRegisterDate, 
               j_users.lastvisitDate as jLastVisitDate, 
               member_msqsite.member_id as mId, 
               member.firstname as mFirstName, 
               member.surname as mSurname, 
               member.number as mMsaNumber,
               member.dob as mDob
        FROM j_users
        LEFT JOIN member_msqsite ON j_users.id = member_msqsite.joomla_uid
        LEFT JOIN member ON member_msqsite.member_id = member.id
        WHERE member_msqsite.member_id IS NOT NULL
        AND (j_users.username LIKE ? OR j_users.name LIKE ?)
        ORDER BY $order_by
        LIMIT ? 
        OFFSET ?";

	$sql_count = "SELECT DISTINCT count(j_users.id)
        FROM j_users
        LEFT JOIN member_msqsite ON j_users.id = member_msqsite.joomla_uid
        LEFT JOIN member ON member_msqsite.member_id = member.id
        WHERE member_msqsite.member_id IS NOT NULL
        AND (j_users.username LIKE ? OR j_users.name LIKE ?);";

} else {

	$sql = "SELECT DISTINCT j_users.id as jId, 
               j_users.name as jName, 
               j_users.username as jUsername, 
               j_users.email as jEmail, 
               j_users.registerDate as jRegisterDate, 
               j_users.lastvisitDate as jLastVisitDate, 
               member_msqsite.member_id as mId, 
               member.firstname as mFirstName, 
               member.surname as mSurname, 
               member.number as mMsaNumber,
               member.dob as mDob
        FROM j_users
        LEFT JOIN member_msqsite ON j_users.id = member_msqsite.joomla_uid
        LEFT JOIN member ON member_msqsite.member_id = member.id
        WHERE (j_users.username LIKE ? OR j_users.name LIKE ?)
        ORDER BY $order_by
        LIMIT ? 
        OFFSET ?";

	$sql_count = "SELECT DISTINCT count(j_users.id)
        FROM j_users
        LEFT JOIN member_msqsite ON j_users.id = member_msqsite.joomla_uid
        LEFT JOIN member ON member_msqsite.member_id = member.id
        WHERE (j_users.username LIKE ? OR j_users.name LIKE ?)";

}

// Allow from any origin
//if (isset($_SERVER['HTTP_ORIGIN'])) {
//
//    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
//    header('Access-Control-Allow-Credentials: true');
//
//}
//
//// Access-Control headers are received during OPTIONS requests
//if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
//
//    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
//        header("Access-Control-Allow-Methods: GET, OPTIONS");
//
//    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
//        header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
//
//}

// Make request and check for errors
$joomlaUsers = $GLOBALS['db']->getAll($sql, array($filterSearch, $filterSearch, $limit, $offset));
db_checkerrors($joomlaUsers);

$totalRows = $GLOBALS['db']->getOne($sql_count, array($filterSearch, $filterSearch));
db_checkerrors($totalRows);

$joomlaUsersProfiles = array();

foreach ($joomlaUsers as $joomla_user) {

	$profile_data = $GLOBALS['db']->getAll( "SELECT * FROM j_user_profiles WHERE user_id = ?",
		array( $joomla_user['jId'] ) );
	db_checkerrors( $profile_data );

	$profile_msa_number = "";
	$profile_dob        = "";
	$profile_club       = "";

	foreach ( $profile_data as $profile_row ) {

		if ( $profile_row['profile_key'] == 'profile.msanumber' ) {
			$profile_msa_number = $profile_row['profile_value'];
		}
		if ( $profile_row['profile_key'] == 'profile.dob' ) {
			$profile_dob = $profile_row['profile_value'];
		}
		if ( $profile_row['profile_key'] == 'profile.club' ) {
			$profile_club = $profile_row['profile_value'];
		}

	}

	$joomlaUserProfile               = $joomla_user;
	$joomlaUserProfile['jMsaNumber'] = str_replace( '"', '', $profile_msa_number );
	$joomlaUserProfile['jDob']       = str_replace( '"', '', $profile_dob );
	$joomlaUserProfile['jClub']      = str_replace( '"', '', $profile_club );

	if ( is_null( $joomla_user['mId'] ) ) {
		$joomlaUserProfile['linked']          = false;
		$joomlaUserProfile['canBeAutoLinked'] = false;
	} else {
		$joomlaUserProfile['linked'] = true;
	}

	// Check if this user is linkable
	if ( is_null( $joomla_user['mId'] ) ) {

		if ( ! empty( $joomlaUserProfile['jMsaNumber'] ) ) {

			$memberDetails = $GLOBALS['db']->getRow( "SELECT * FROM member WHERE number = ?",
				array( $joomlaUserProfile['jMsaNumber'] ) );
			db_checkerrors( $memberDetails );

			if ( $memberDetails['id'] != 0 ) {

				$joomlaUserProfile['mMsaNumber'] = $memberDetails['number'];
				$joomlaUserProfile['mFirstName'] = $memberDetails['firstname'];
				$joomlaUserProfile['mSurname']   = $memberDetails['surname'];
				$joomlaUserProfile['mDob']       = $memberDetails['dob'];
				$joomlaUserProfile['mId']        = $memberDetails['id'];

				if ( $joomlaUserProfile['mDob'] == $joomlaUserProfile['jDob'] ) {
					$joomlaUserProfile['canBeAutoLinked'] = true;
				}

			} else {

				$memberDetails = $GLOBALS['db']->getAll( "SELECT * FROM member 
								WHERE MATCH(firstname, surname, othernames) AGAINST (?)",
					array( $joomlaUserProfile['jName'] ) );
				db_checkerrors( $memberDetails );

				$firstMatch = $memberDetails[0];

				$joomlaUserProfile['mMsaNumber'] = $firstMatch['number'];
				$joomlaUserProfile['mFirstName'] = $firstMatch['firstname'];
				$joomlaUserProfile['mSurname']   = $firstMatch['surname'];
				$joomlaUserProfile['mDob']       = $firstMatch['dob'];
				$joomlaUserProfile['mId']        = $memberDetails['id'];

				if ( ( ! empty( $firstMatch['number'] ) )
				     && $joomlaUserProfile['mDob'] == $joomlaUserProfile['jDob'] ) {
					$joomlaUserProfile['canBeAutoLinked'] = true;
				}

			}
		} else {

			$memberDetails = $GLOBALS['db']->getAll( "SELECT * FROM member 
								WHERE MATCH(firstname, surname, othernames) AGAINST (?)",
				array( $joomlaUserProfile['jName'] ) );
			db_checkerrors( $memberDetails );

			$firstMatch = $memberDetails[0];

			$joomlaUserProfile['mMsaNumber'] = $firstMatch['number'];
			$joomlaUserProfile['mFirstName'] = $firstMatch['firstname'];
			$joomlaUserProfile['mSurname']   = $firstMatch['surname'];
			$joomlaUserProfile['mDob']       = $firstMatch['dob'];
			$joomlaUserProfile['mId']        = $memberDetails['id'];

			if ( ( ! empty( $firstMatch['number'] ) )
			     && $joomlaUserProfile['mDob'] == $joomlaUserProfile['jDob'] ) {
				$joomlaUserProfile['canBeAutoLinked'] = true;
			}

		}

		// Get Clubs
		if ( ! is_null( $joomlaUserProfile['mId'] ) ) {

			$memberId = $joomlaUserProfile['mId'];

			$memberClubs = $GLOBALS['db']->getAll("SELECT * FROM member_memberships 
					WHERE member_id = '$memberId'
					GROUP BY club_id;");
			db_checkerrors($memberClubs);

			$clubList      = array();

			if ( isset( $memberClubs ) && is_array( $memberClubs ) ) {

				foreach ( $memberClubs as $memberClub ) {

					$clubId = $memberClub['club_id'];

					$clubDetails = $GLOBALS['db']->getRow("SELECT * FROM clubs WHERE id = ?", array($clubId));
					db_checkerrors($clubDetails);

					$clubDetails['id']            = $clubDetails['id'];
					$clubDetails['code']          = $clubDetails['code'];
					$clubDetails['clubName']      = $clubDetails['clubname'];

					$shortenedName = $clubDetails['clubname'];
					$shortenedName = str_replace(" Club", "", $shortenedName);
					$shortenedName = str_replace(" Swimming", "", $shortenedName);
					$shortenedName = str_replace(" Swim", "", $shortenedName);
					$shortenedName = str_replace(" AUSSI", "", $shortenedName);
					$shortenedName = str_replace(" Inc", "", $shortenedName);
					$shortenedName = trim($shortenedName);

					$clubDetails['clubNameShort'] = $shortenedName;

					$clubList[] = $clubDetails;

				}

			}

			$joomlaUserProfile['mClubs'] = $clubList;

		}

		$joomlaUsersProfiles[] = $joomlaUserProfile;
	}

}

$response_obj = array(
	'count' => $totalRows,
	'data'  => $joomlaUsersProfiles
);

// Send JSON Response
header( 'Content-type: application/json' );
$callback = filter_input( INPUT_GET, 'callback', FILTER_SANITIZE_SPECIAL_CHARS );

if ( isset( $_GET['callback'] ) ) {

	echo $callback . '(' . json_encode( $response_obj ) . ');';

} else {

	echo json_encode( $response_obj );

}

?>