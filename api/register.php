<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/12/17
 * Time: 3:49 PM
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/swimman/vendor/autoload.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/swimman/api/common.php");

use \Firebase\JWT\JWT;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

header('Access-Control-Allow-Origin: *');

// Authentication logger
$log_level = Logger::DEBUG;
$apiLog = new Logger('api');
$apiLog->pushProcessor(new \Monolog\Processor\WebProcessor);
$apiLog->pushHandler(new StreamHandler($_SERVER['DOCUMENT_ROOT'] . "/logs/" . 'api.log', $log_level));

$pdo = getPDO();

// Check if this username and password exist on User table
$statement = $pdo->prepare('SELECT * FROM users 
	WHERE username = :username OR email = :email;');
$statement->execute([
	':username' => $username,
	':email' => $username
]);

$row = $statement->fetch(PDO::FETCH_ASSOC);
$userdetails = [];

if (isset($row['passwordhash'])) {

	// User already has a new user table entry

	// Get algorithm, hash and salt
	$hashed = str_replace(')', '', $row['passwordhash']);
	list($algo, $payload) = explode('(', $hashed);
	list($hash, $salt) = explode(':', $payload);

	$testhash = hash($algo, ($password . $salt));

	if ($hash != $testhash) {``

		header( 'HTTP/1.1 401 Unauthorized', true, 401 );
		$apiLog->error("Authentication failure for $username``$apiLog->info( "User login by $username via Swimmman user" );

	unset($row['passwordhash']);

	$userdetails = $row;

} else {

	$statement = $pdo->prepare( 'SELECT * FROM j_users 
	WHERE username = :username OR email = :email;' );
	$statement->execute( [
		':username' => $username,
		':email'    => $username
	] );

	$row = $statement->fetch( PDO::FETCH_ASSOC );


	if ( isset( $row['password'] ) ) {

		list( $md5pass, $md5salt ) = explode( ':', $row['password'] );
		$userhash = md5( $password . $md5salt );

		if ( $md5pass != $userhash ) {

			header( 'HTTP/1.1 401 Unauthorized', true, 401 );
			$apiLog->error( "Authentication failure for $username" );
			exit();

		} else {

			$apiLog->info( "User login by $username via Joomla user" );

		}

	} else {
		header( 'HTTP/1.1 401 Unauthorized', true, 401 );
		$apiLog->error( "User profile not found for $username" );
		exit();
	}

	// If they're authenticated with Joomla, get the Joomla email address
	$email = $row['email'];

	// Check if this user is associated with a member
	$joomla_uid = $row['id'];

	$statement = $pdo->prepare( 'SELECT * FROM member_msqsite 
	WHERE joomla_uid = :joomla_uid;' );
	$statement->execute( [
		':joomla_uid' => $joomla_uid,
	] );

	$row = $statement->fetch( PDO::FETCH_ASSOC );

	$member = "";
	$firstname = "";
	$surname = "";
	if ( $row ) {
		$member = $row['member_id'];

		$statement = $pdo->prepare( 'SELECT * FROM member 
			WHERE id = :member_id;' );
		$statement->execute( [
			':member_id' => $member,
		] );

		$row = $statement->fetch( PDO::FETCH_ASSOC );

		if ($row) {

			$firstname = $row['firstname'];
			$surname = $row['surname'];

		}

	}

	$salt_new = base64_encode(openssl_random_pseudo_bytes(32));
	$passwordhash_new = 'sha256(' . hash('sha256', $password . $salt_new) . ':' . $salt_new . ')';

	// Create user table entry
	$statement = $pdo->prepare('INSERT INTO users 
		(member, username, passwordhash, lastupdated, firstname, surname, email)
		VALUES (:member_id, :username, :password, NOW(), :firstname, :surname, :email);');
	$statement->execute( [
		':member_id' => $member,
		':username' => $username,
		':password' => $passwordhash_new,
		':firstname' => $firstname,
		':surname' => $surname,
		':email' => $email
	]);

	$user_Id = $pdo->lastInsertId();

	$statement = $pdo->prepare("SELECT id, member, username, lastupdated, firstname, surname, email FROM users WHERE id = :user_id;");
	$statement->execute( [
		":user_id" => $user_id
	]);

	$userdetails = $statement->fetch( PDO::FETCH_ASSOC );

}

$key = "example_key";

$token = array(
	"iss" => "https://forum.mastersswimmingqld.org.au",
	"aud" => "https://forum.mastersswimmingqld.org.au",
	"iat" => time(),
    "nbf" => time(),
	"user" => $userdetails['id'],
);

$jwt["token"] = JWT::encode($token, $key);
$jwt["user"] = $userdetails;

header('Content-type: application/json');
echo json_encode($jwt);