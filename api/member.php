<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 23/12/17
 * Time: 12:08 PM
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/swimman/vendor/autoload.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/swimman/api/common.php");

use \Firebase\JWT\JWT;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// Authentication logger
$log_level = Logger::DEBUG;
$apiLog = new Logger('member');
$apiLog->pushProcessor(new \Monolog\Processor\WebProcessor);
$apiLog->pushHandler(new StreamHandler($_SERVER['DOCUMENT_ROOT'] . "/logs/" . 'api.log', $log_level));

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	// return only the headers and not the content
	// only allow CORS if we're doing a GET - i.e. no saving for now.
	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']) &&
	    $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'] == 'GET') {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Authorization');
	}
	exit;
}

$headers = getallheaders();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Authorization');

if (isset($headers['Authorization'])) {

	$token = str_replace('Bearer ', '', $headers['Authorization']);

} else {

	header( 'HTTP/1.1 401 Unauthorized', true, 401 );
	$apiLog->error("Attempt to access member details service without token");
	exit();

}

$authData;

try {

	$key = "example_key";
	$authData = JWT::decode($token, $key, array('HS256'));

} catch (Exception $e) {

	header( 'HTTP/1.1 401 Unauthorized', true, 401 );
	$apiLog->error("Unauthorized attempt to access member details service");
	exit();

}

if (isset($authData->user)) {

	$user_id = $authData->user;

	// TODO: have capacity for users to have permissions to get member details of other members

	$pdo = getPDO();

	// Get member ID
	$statement = $pdo->prepare("SELECT member FROM users WHERE id = :id");
	$statement->execute([
		':id' => $user_id,
	]);

	$user_data = $statement->fetch(PDO::FETCH_ASSOC);

	if (!$user_data || $user_data['member'] == 0) {
		header('Content-type: application/json');
		echo "{}";
		exit();
	}

	$member_id = $user_data['member'];

	// Get member details
	$statement = $pdo->prepare("SELECT id, number, surname, firstname, 
 		othernames, dob, 
 		IF(gender = 1, 'M', 'F') as gender FROM member WHERE id = :id;");
	$statement->execute([
		':id' => $member_id,
	]);

	$member_data = $statement->fetch(PDO::FETCH_ASSOC);

	// Get memberships
	$statement = $pdo->prepare("SELECT m.id, m.club_id, c.clubname, c.code, t.typename, s.desc, m.startdate, m.enddate
FROM member_memberships as m, clubs as c, membership_types as t, membership_statuses as s
WHERE member_id = :id
  AND m.club_id = c.id
  AND m.status = s.id
  AND m.type = t.id
ORDER BY enddate DESC;");
	$statement->execute([
		':id' => $member_id,
	]);

	$membership_data = $statement->fetchAll();

	$member_data['memberships'] = $membership_data;
	$member_json = json_encode($member_data);

	header('Content-type: application/json');
	echo $member_json;

	$apiLog->info("Served information on " .  $member_data['firstname'] . ' ' .
	              $member_data['surname'] . '(' . $member_id .
		')');

}