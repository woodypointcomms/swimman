<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 19/10/18
 * Time: 6:54 AM
 */
empty( $_SERVER['DOCUMENT_ROOT'] ) && $_SERVER['DOCUMENT_ROOT'] = "/hsphere/local/home/davsoft/forum.mastersswimmingqld.org.au";

require_once ($_SERVER['DOCUMENT_ROOT'] . "/swimman/api/common.php");

$fieldname = 'uploadfile';
$targetdir = $_SERVER['DOCUMENT_ROOT'] . '/../masters-tmp/';

$return=array(
	'success'    =>    'false',
	'message'    =>    'File not uploaded!'
);

if (!is_user_logged_in()) {
	header( 'HTTP/1.1 401 Unauthorized', true, 401 );
	$apiLog->error("Attempt to access file upload service without valid token");
	exit('HTTP/1.1 401 Unauthorized');
}

if( $_SERVER['REQUEST_METHOD']=='POST' && isset( $_FILES[ $fieldname ] ) ){
	try{

		$obj = (object)$_FILES[ $fieldname ];
		$name = $obj->name;
		$size = $obj->size;
		$tmp  = $obj->tmp_name;
		$type = $obj->type;
		$error= $obj->error;

		if( is_uploaded_file( $tmp ) && $error == UPLOAD_ERR_OK ) {

			$destination = $targetdir . $name;

			if( file_exists( $destination ) ){

				$return['success'] = 'false';
				$return['message'] = 'File already exists!';
				$return['line'] = __LINE__;

				clearstatcache();

			} else {

				$res = move_uploaded_file( $tmp, $destination );
				$return['success'] = $res ? 'true' : false;
				$return['message'] = $res ? 'File uploaded successfully!' : 'Unable to store file!';

			}

		} else {
			$return['success'] = 'false';
			$return['message'] = 'File upload error!';
			$return['line'] = __LINE__;
		}

	} catch( Exception $e ) {
		$return['success'] = 'false';
		$return['message'] = $e->getMessage();
		$return['line'] = __LINE__;
	}

	header('Content-type: application/json');
	exit( json_encode( $return ) );

} else {
	header( 'HTTP/1.1 405 Method Not Allowed',true, 405 );
	exit('HTTP/1.1 405 Method Not Allowed');
}
