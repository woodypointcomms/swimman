<?php
/**
 * Converts entries to Dynamo DB
 *
 * User: david
 * Date: 21/8/17
 * Time: 9:40 AM
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/swimman/includes/setup.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/swimman/vendor/autoload.php');

use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\DynamoDb\Marshaler;

$client = new DynamoDbClient([
	'region'  => 'ap-southeast-2',
	'version' => 'latest',
	'credentials' => [
		'key'    => $GLOBALS['dynamodb'],
		'secret' => $GLOBALS['dynamodbsecret'],
	],
]);

$tableName = 'Meets';

$marshaler = new Marshaler();

$meets = $GLOBALS['db']->getAll("SELECT * FROM meet ORDER BY startdate;");
db_checkerrors($meets);

foreach ($meets as $m) {

	$mId = $m[0];
	$meetName = $m[1];
	$startDate = $m[2];
	$endDate = $m[3];
	$deadline = $m[4];

	$entries = $GLOBALS['db']->getAll("SELECT * FROM meet_entries WHERE meet_id = ?;", array($mId));
	db_checkerrors($entries);

	$entryList = array();

	foreach ($entries as $e) {

		$entryId = $e[0];
		$memberId = $e[1];

		$entryItem = array(
				"entry_id" => $entryId,
				"member_id" => $memberId
		);

		$entryList[] = json_encode($entryItem);

	}

	$entryData = json_encode($entryList);

	try {
		$item = $marshaler->marshalJson( '
	    {
	        "meet_id": ' . $mId . ',
	        "name": "' . $meetName . '",
	        "startDate": "' . $startDate . '",
	        "endDate": "' . $endDate . '",
	        "deadline": "' . $deadline . '",
	        "entries": "' . $entryData . '"
	    }
	' );

	} catch (Exception $e) {
		
		echo $e->getMessage();
		
	}

	$params = array(
		'TableName' => $tableName,
		'Item' => $item
	);


	try {
		$result = $client->putItem($params);
		echo "Added item: $meetName($mId)\n";

	} catch (DynamoDbException $e) {
		echo "Unable to add item:\n";
		echo $e->getMessage() . "\n";
	}

}