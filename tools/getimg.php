<?php
// Gets latest QLD registrations from the IMG Console
empty( $_SERVER['DOCUMENT_ROOT'] ) && $_SERVER['DOCUMENT_ROOT'] = "/hsphere/local/home/davsoft/forum.mastersswimmingqld.org.au/";
require_once(realpath( dirname( __FILE__ ) ) . "/../includes/imgfunctions.php");
require_once(realpath( dirname( __FILE__ ) ) . "/../includes/classes/SlackNotification.php");

$sn = new SlackNotification();

$sn->send_text('Automatic update of membership data from SportsTG started.');

// Retrieve the IMG Data
getImg();

// Parse the IMG Data
parseImg();

$sn->send_text('Automatic update of membership data from SportsTG completed.');
